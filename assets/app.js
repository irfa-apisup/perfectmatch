/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import "./js/libs/hinclude";
import "select2/dist/js/select2.full.min.js";
// import 'select2/dist/css/select2.min.css'
// import '../public/bundles/tetranzselect2entity/js/select2entity'

import "select2/dist/css/select2.min.css";
import "select2-bootstrap-5-theme/dist/select2-bootstrap-5-theme.min.css";

import "litepicker-polyfills-ie11";
import Litepicker from "litepicker";

import "./styles/variables.css";
import "./styles/app.css";

$(".select2-test").select2({
  theme: "bootstrap-5",
});

$(".select2-single").select2({
  theme: "bootstrap-5",
  minimumResultsForSearch: -1,
});

$(".select2-multiple").select2({
  theme: "bootstrap-5",
  minimumResultsForSearch: -1,
});

$(".select2-multiple-no-search").select2({
  theme: "bootstrap-5",
  minimumResultsForSearch: -1,
});

$(".select2-multiple-no-search").on(
  "select2:opening select2:closing",
  function (event) {
    const $searchfield = $(this).parent().find(".select2-search--inline");
    $searchfield.remove();
  }
);

jQuery(document).ready(function () {
  const list = $("#images-fields-list");
  if (list !== undefined && list.attr("data-prototype") !== undefined) {
    jQuery(".add-another-collection").click(function (e) {
      const list = $("#images-fields-list");
      let counter = list.data("widget-counter") | list.children().length;
      let newWidget = list.attr("data-prototype");
      newWidget = newWidget.replace(/__name__/g, counter);
      counter++;
      list.data("widget-counter", counter);

      let newElem = jQuery(list.attr("data-widget-tags")).html(newWidget);
      newElem.appendTo(list);
      newElem.append('<i class="fas fa-times-circle remove-tag"></i>');
      $(".remove-tag").click(function (e) {
        e.preventDefault();

        $(this).parent().remove();
      });
      initImages();
    });
  }
});

initImages();
$(".remove-tag").click(function (e) {
  e.preventDefault();

  $(this).parent().parent().remove();
});

/* Image preview */
function getImgData(file) {
  const files = file.files[0];
  const imgPreview = document.querySelectorAll(
    "[for='" + file.getAttribute("id") + "']"
  )[0];
  if (files) {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(files);
    fileReader.addEventListener("load", function () {
      if (imgPreview.firstElementChild != null) {
        imgPreview.removeChild(imgPreview.firstElementChild);
      }
      let element = document.createElement("img");
      element.src = this.result;
      element.classList.add("img-preview");
      imgPreview.appendChild(element);
    });
  }
}

function initImages() {
  const chooseFiles = document.getElementsByClassName("images-field");
  for (let i = 0; i < chooseFiles.length; i++) {
    const chooseFile = chooseFiles[i];
    console.log(chooseFile);
    chooseFile.addEventListener("change", function () {
      console.log("change");
      getImgData(chooseFile);
    });
  }
}

initPicker();

function initPicker() {
  let periodContainer = document.getElementsByClassName("date-range");

  for (let i = 0; i < periodContainer.length; i++) {
    const period = periodContainer[i];
    let childs = period.childNodes;
    let start = childs[1].childNodes[1];
    let end = childs[3].childNodes[1];

    let picker = new Litepicker({
      element: start,
      elementEnd: end,
      singleMode: false,
      resetButton: true,
    });
  }
}

/* Documents collection */
jQuery(document).ready(function () {
  const list = $("#documents-fields-list");
  jQuery(".add-another-collection").click(function (e) {
    const list = $("#documents-fields-list");
    let counter = list.data("widget-counter") | list.children().length;
    let newWidget = list.attr("data-prototype");
    if (list !== undefined && list.attr("data-prototype") !== undefined) {
      newWidget = newWidget.replace(/__name__/g, counter);
      counter++;
      list.data("widget-counter", counter);
      let newElem = jQuery(list.attr("data-widget-tags")).html(newWidget);
      newElem.appendTo(list);
      $(".remove-tag").click(function (e) {
        e.preventDefault();
        $(this).parent().remove();
      });
    }
  });
});

$(".remove-tag-single").click((e) => {
  e.preventDefault();
  const id = e.target.getAttribute("data-delete");
  const element = document.getElementById(id);
  if (element) {
    element.value = "True";
    const parent = e.target.parentNode;
    e.target.parentNode.parentNode.removeChild(parent);
  }
});

const formatModal = (target) => {
  const id = target.getAttribute("data-id");
  const name = target.getAttribute("data-name");
  const modal = document.getElementById("reportModalForm");
  const label = document.getElementById("reportModalLabel");
  const text = label.getAttribute("data-text");
  const action = modal.getAttribute("data-action");
  label.innerText = text + ` (${name})`;
  modal.action = action.replace("-1", id);
};

const reportModal = document.getElementById("reportModalButton");

if (reportModal !== null) {
  reportModal.addEventListener("click", (e) => {
    formatModal(e.currentTarget);
  });
}

const showReportModal = document.getElementById("reportingShow");

if (showReportModal !== null) {
  showReportModal.addEventListener("click", (e) => {
    const text = document.getElementById("reportingMessage");
    const message = e.currentTarget.getAttribute("data-message");
    text.innerText = message;
  });
}

const showPasswordList = $(".viewPassword");
const hidePasswordList = $(".hidePassword");
if ((showPasswordList && hidePasswordList) !== null) {
  for(let i = 0; i < showPasswordList.length; i++){
    const showPassword = $(showPasswordList[i]);

    showPassword.on('click', () => {
      const sibling = showPassword.siblings();
      const inputPassword = $(sibling[0]);
      const hidePassword = $(sibling[1]);

      showPassword.addClass('hidden')
      hidePassword.removeClass('hidden')
      inputPassword.attr('type', 'text')
    })
  }

  for(let i = 0; i < hidePasswordList.length; i++){
    const hidePassword= $(hidePasswordList[i]);

    hidePassword.on('click', () => {
      const sibling = hidePassword.siblings();
      const inputPassword = $(sibling[0]);
      const showPassword = $(sibling[1]);

      showPassword.removeClass('hidden')
      hidePassword.addClass('hidden')
      inputPassword.attr('type', 'password')
    })
  }
}
