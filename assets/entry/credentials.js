import '../app.js'

import '../styles/credentials/login.css'

$('.school-select').change(function () {
    let schoolSelector = $(this);
    let degreesSelect = $(".degree-list");
    let label =  document.querySelectorAll("[for='" + degreesSelect.attr('id') + "']")[0];
    degreesSelect.html('');
    degreesSelect.append('<option>...</option>');
    // Request the neighborhoods of the selected city.
    $.ajax({
        url: "/degrees/" + schoolSelector.val(),
        type: "GET",
        dataType: "JSON",
        success: function (degrees) {

            degreesSelect.html('');
            degreesSelect.append('<option>' + label.innerHTML + '</option>');
            $.each(degrees, function (key, degree) {
                degreesSelect.append('<option value="' + degree.id + '">' + degree.name + '</option>');
            });
        },
        error: function (err) {
            degreesSelect.html('');
        }
    });
})

jQuery(document).ready(function () {
    jQuery('#add-period').click(function (e) {
        var list = $(".period-list");
        var counter = list.data('widget-counter') | list.children().length;
        var newWidget = list.attr('data-prototype');
        newWidget = newWidget.replace(/__name__/g, counter);
        counter++;
        list.data('widget-counter', counter);

        var newElem = jQuery(list.attr('data-widget-tags')).html(newWidget);
        newElem.appendTo(list);
        initPicker();
        $('.remove-period').click(function (e) {
            e.preventDefault();

            $(this).parent().parent().parent().remove();

        });
    });

});

$('.remove-period').click(function (e) {
    e.preventDefault();

    $(this).parent().parent().parent().remove();

});
initPicker();
function initPicker() {
    let periodContainer = document.getElementsByClassName('period-container');

    for (let i = 0; i < periodContainer.length; i++) {
        const period = periodContainer[i];
        let childs = period.childNodes;
        let start = childs[1].childNodes[1];
        let end = childs[3].childNodes[1];

        let picker = new Litepicker({
            element: start,
            elementEnd: end,
            singleMode: false,
            allowRepick: true,
        })
        console.log(picker)
    }
}
