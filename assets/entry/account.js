import '../app.js'
import '../js/admin/settings.js'

import '../styles/account/layout.css'
import '../styles/account/dashboard.css'
import '../styles/credentials/login.css'
import '../styles/account/settings.css'
import '../styles/account/messaging.css'
import '../styles/account/crud.css'

import '../styles/admin/settings.css'

let sidebar = document.getElementsByClassName('movable')[0]
let sidebarToogleButton = document.getElementsByClassName('navbar-toogle')

let clickFunc = () => {
    if (sidebar.classList.contains('sidebarShow')) {
        sidebar.classList.remove("sidebarShow")
    } else {
        sidebar.classList.add("sidebarShow")
    }
}

for (let i = 0; i < sidebarToogleButton.length; i++) {
    sidebarToogleButton[i].addEventListener('click', clickFunc)
}

//Accoutn settings
let settingsNavItems = document.getElementsByClassName('settings-nav-item')
let settingsNavBarItems = document.getElementsByClassName('settings-sidebar-item')

//Sidebar

for (let i = 0; i < settingsNavBarItems.length; i++) {
    let element = settingsNavBarItems[i]

    let targetId = element.getAttribute('data-target')
    let el = document.getElementById(targetId)

    let bodyTargeted = document.getElementById(`settings-${targetId}`)

    element.addEventListener('click', () => {
        removeAllActive(settingsNavItems)
        resetSettingsDynamic()

        if (el != undefined) {
            el.classList.add('active')
            console.log('add')
        }

        if (bodyTargeted != undefined) {
            bodyTargeted.classList.remove('hidden')
        }
    })

    if (element.getAttribute('data-target') == window.location.href.split('#')[1] && el != undefined) {
        el.classList.add('active')
        bodyTargeted.classList.remove('hidden')
    }


}
//Settings navbar

for (let i = 0; i < settingsNavItems.length; i++) {
    let element = settingsNavItems[i]
    let targetId = element.getAttribute('data-target')
    console.log(element)

    let bodyTargeted = document.getElementById(`settings-${targetId}`)
    element.addEventListener('click', () => {
        removeAllActive(settingsNavItems)
        resetSettingsDynamic()
        element.classList.add('active')
        bodyTargeted.classList.remove('hidden')

    })
}

let removeAllActive = (array) => {
    for (let i = 0; i < array.length; i++) {
        let element = array[i]

        element.classList.remove('active')
    }
}

let resetSettingsDynamic = () => {
    let elements = document.getElementsByClassName('settings-dynamic')
    for (let i = 0; i < elements.length; i++) {
        let el = elements[i]
        el.classList.add('hidden')
    }
}

//Modal delete
let deleters = document.getElementsByClassName('deleter')

for (let i = 0; i < deleters.length; i++) {
    const element = deleters[i];
    let target = document.getElementById(element.getAttribute('data-target'))
    element.addEventListener('click', () => {
        target.submit();
    })
}

/**
 * Trainee school ajax degree
 */
 $('.school-select').change(function () {
    let schoolSelector = $(this);
    let degreesSelect = $(".degree-list");
    let label =  document.querySelectorAll("[for='" + degreesSelect.attr('id') + "']")[0];
    degreesSelect.html('');
    degreesSelect.append('<option>...</option>');
    // Request the neighborhoods of the selected city.
    $.ajax({
        url: "/degrees/" + schoolSelector.val(),
        type: "GET",
        dataType: "JSON",
        success: function (degrees) {

            degreesSelect.html('');
            degreesSelect.append('<option>' + label.innerHTML + '</option>');
            $.each(degrees, function (key, degree) {
                degreesSelect.append('<option value="' + degree.id + '">' + degree.name + '</option>');
            });
        },
        error: function (err) {
            degreesSelect.html('');
        }
    });
})


jQuery(document).ready(function () {
    jQuery('#add-period').click(function (e) {
        var list = $(".period-list");
        var counter = list.data('widget-counter') | list.children().length;
        var newWidget = list.attr('data-prototype');
        newWidget = newWidget.replace(/__name__/g, counter);
        counter++;
        list.data('widget-counter', counter);

        var newElem = jQuery(list.attr('data-widget-tags')).html(newWidget);
        newElem.appendTo(list);
        periodPicker();
        $('.remove-period').click(function (e) {
            e.preventDefault();

            $(this).parent().parent().parent().remove();

        });
    });

});

$('.remove-period').click(function (e) {
    e.preventDefault();

    $(this).parent().parent().parent().remove();
});
periodPicker();
function periodPicker() {
    let periodContainer = document.getElementsByClassName('period-container');

    for (let i = 0; i < periodContainer.length; i++) {
        const period = periodContainer[i];
        let childs = period.childNodes;
        let start = childs[1].childNodes[1];
        let end = childs[3].childNodes[1];

        let picker = new Litepicker({
            element: start,
            elementEnd: end,
            singleMode: false,
            allowRepick: true,
        })
        console.log(picker)
    }
}
