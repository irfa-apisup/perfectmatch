import '../app.js'
import '../js/main/survey.js'
import '../js/error.js'

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.min.css';
import 'owl.carousel';

import '../styles/error.css'
import '../styles/main/index.css'
import '../styles/main/referer.css'
import '../styles/main/survey.css'
import '../styles/main/contact.css'
import '../styles/main/search.css'
import '../styles/main/toolbox.css'
import '../styles/main/term.css'

(function ($) {

    "use strict";

    let fullHeight = function () {

        $('.js-fullheight').css('height', $(window).height());
        $(window).resize(function () {
            $('.js-fullheight').css('height', $(window).height());
        });

    };
    fullHeight();

    let carousel = function () {
        let owl = $('.featured-carousel').owlCarousel({
            autoplay: true,
            autoplayTimeout: 3500,
            margin: 26,
            nav: false,
            dots: true,
            items: 1,
            responsive: {
                0: {
                    items: 1
                },
                992: {
                    items: 2
                },
                1300: {
                    items: 3
                }
            }
        });

        owl.on('changed.owl.carousel', function (e) {
            owl.trigger('stop.owl.autoplay');
            owl.trigger('play.owl.autoplay');
        });

    };
    carousel();

})(jQuery);