const axios = require('axios').default;

let switchList = document.getElementsByClassName('settings-item-switch')

for (let i = 0; i < switchList.length; i++) {
    const s = switchList[i];
    s.addEventListener('change', (e) => {
        let check = s.checked;
        let route = s.getAttribute('data-route').replace('state', check);
        console.log(check);
        if((route && check) != undefined){
            axios.get(route, {
                params: {
                  state: check
                }
              })
              .then(function (response) {
                console.log(response);
              })
              .catch(function (error) {
                console.log(error);
              })
              .then(function () {
                // always executed
              });
        }
    })
}
