let next = document.getElementById('survey-next')
let prev = document.getElementById('survey-prev')
let surveyContainer = document.getElementById('survey-container')
let progress = document.getElementById('survey-progress')
let dataSurveyContainer = document.getElementById('survey-data-container')

if (surveyContainer != undefined) {
    const MAXSTEP = parseInt(surveyContainer.getAttribute('data-max-length'))
    const PROGRESS_OFFSET = Math.ceil(100 / MAXSTEP)
    progress.style.width = PROGRESS_OFFSET + "%"
    dataSurveyContainer.style.marginLeft = '0%'

    let step1 = document.querySelector('.survey-data-step[data-step="1"]');
    let step2 = document.querySelector('.survey-data-step[data-step="2"]');
    let step3 = document.querySelector('.survey-data-step[data-step="3"]');
    let step4 = document.querySelector('.survey-data-step[data-step="4"]');
    let step5 = document.querySelector('.survey-data-step[data-step="5"]');

    next.addEventListener('click', (e) => {
        let currentCountStep = surveyContainer.getAttribute('data-current-step')
        let nextCount = parseInt(currentCountStep) + 1;
        let cstep = document.querySelector('.survey-data-step[data-step="' + currentCountStep + '"]');
        let tstep = document.querySelector('.survey-data-step[data-step="' + nextCount + '"]');

        if (currentCountStep == MAXSTEP) {
            let form = document.getElementById(next.getAttribute('data-form'));
            form.submit();
        }

        if ((cstep && tstep) != undefined) {
            let cMargin = parseInt(dataSurveyContainer.style.marginLeft.replace('%', ''))
            dataSurveyContainer.style.marginLeft = (cMargin + -100) + '%'
            surveyContainer.setAttribute('data-current-step', nextCount)
            let cProgress = parseInt(progress.style.width.replace('%', ''))
            progress.style.width = (cProgress + PROGRESS_OFFSET) + '%'
            if (nextCount == MAXSTEP) {
                next.innerHTML = next.getAttribute('data-label-finished')
                next.classList.add('survey-finished')
            }
        }
    })

    prev.addEventListener('click', (e) => {
        let currentCountStep = surveyContainer.getAttribute('data-current-step')
        let lastCount = parseInt(currentCountStep) - 1;
        let cstep = document.querySelector('.survey-data-step[data-step="' + currentCountStep + '"]');
        let tstep = document.querySelector('.survey-data-step[data-step="' + lastCount + '"]');

        if ((cstep && tstep) != undefined) {
            let cMargin = parseInt(dataSurveyContainer.style.marginLeft.replace('%', ''))
            dataSurveyContainer.style.marginLeft = (cMargin - -100) + '%'
            surveyContainer.setAttribute('data-current-step', lastCount)
            let cProgress = parseInt(progress.style.width.replace('%', ''))
            progress.style.width = (cProgress - PROGRESS_OFFSET) + '%'
            if (currentCountStep == MAXSTEP) {
                next.innerHTML = next.getAttribute('data-label-next')
                next.classList.remove('survey-finished')
            }
        }
    })

    let badges = document.getElementsByClassName('survey-badge')
    for (let i = 0; i < badges.length; i++) {
        const badge = badges[i];
        badge.addEventListener('click', (e) => {
            addValue(badge, badge.innerHTML)
            removeActiveBadge(badge);
            badge.classList.add('active');
        })
    }
}

const removeActiveBadge = (target) => {
    let parent = target.parentNode
    let children = parent.children
    for (let i = 0; i < children.length; i++) {
        const badge = children[i];
        if (badge.classList.contains('active')) {
            badge.classList.remove('active')
        }
    }
}

const addValue = (target, value) => {
    let parent = target.parentNode
    let input = parent.lastElementChild
    input.value = value
}

/** Survey */
let surveyToastEl = document.getElementById('survey-toast')
var surveyToast = bootstrap.Toast.getOrCreateInstance(surveyToastEl)

let surveyToastClose = document.getElementById('survey-toast-close');
if(surveyToastClose != undefined){
    surveyToastClose.addEventListener('click', () => {
        surveyToast.hide()
    })
}