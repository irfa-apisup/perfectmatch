<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220314103158 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE compagny (id INT NOT NULL, name VARCHAR(255) NOT NULL, contact_firstname VARCHAR(255) DEFAULT NULL, contact_lastname VARCHAR(255) DEFAULT NULL, contact_mail VARCHAR(255) DEFAULT NULL, sector VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, postalcode VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, contact_phone VARCHAR(255) NOT NULL, website VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, logofilename VARCHAR(255) DEFAULT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE compagny_image (id INT AUTO_INCREMENT NOT NULL, compagny_id INT DEFAULT NULL, filename VARCHAR(255) DEFAULT NULL, updated_at DATETIME NOT NULL, INDEX IDX_42A0E791224ABE0 (compagny_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE compagny_internship (id INT AUTO_INCREMENT NOT NULL, compagny_id INT NOT NULL, title VARCHAR(255) NOT NULL, working_time VARCHAR(255) DEFAULT NULL, location VARCHAR(255) NOT NULL, contract_type VARCHAR(255) NOT NULL, vacancies INT NOT NULL, description LONGTEXT DEFAULT NULL, search_profil LONGTEXT DEFAULT NULL, start_period DATETIME NOT NULL, end_period DATETIME NOT NULL, prerequisite LONGTEXT DEFAULT NULL, another LONGTEXT DEFAULT NULL, active TINYINT(1) NOT NULL, INDEX IDX_FA65D9361224ABE0 (compagny_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE degree (id INT AUTO_INCREMENT NOT NULL, school_id INT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, active TINYINT(1) NOT NULL, INDEX IDX_A7A36D63C32A47EE (school_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE internship_period (id INT AUTO_INCREMENT NOT NULL, trainee_id INT NOT NULL, start DATETIME NOT NULL, end DATETIME NOT NULL, INDEX IDX_56D813A36C682D0 (trainee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE school (id INT NOT NULL, name VARCHAR(255) NOT NULL, contact_firstname VARCHAR(255) DEFAULT NULL, contact_lastname VARCHAR(255) DEFAULT NULL, contact_mail VARCHAR(255) DEFAULT NULL, address VARCHAR(255) NOT NULL, postalcode VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, contact_phone VARCHAR(255) NOT NULL, updated_at DATETIME NOT NULL, description LONGTEXT DEFAULT NULL, website VARCHAR(255) DEFAULT NULL, logofilename VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE school_image (id INT AUTO_INCREMENT NOT NULL, school_id INT DEFAULT NULL, filename VARCHAR(255) DEFAULT NULL, updated_at DATETIME NOT NULL, INDEX IDX_1BC7B245C32A47EE (school_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trainee (id INT NOT NULL, school_id INT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, sector VARCHAR(255) NOT NULL, website VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, other LONGTEXT DEFAULT NULL, link VARCHAR(255) DEFAULT NULL, objectif LONGTEXT DEFAULT NULL, zone VARCHAR(255) DEFAULT NULL, cvfilename VARCHAR(255) DEFAULT NULL, avatarfilename VARCHAR(255) DEFAULT NULL, languages VARCHAR(255) DEFAULT NULL, INDEX IDX_46C68DE7C32A47EE (school_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, active TINYINT(1) NOT NULL, discr VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE compagny ADD CONSTRAINT FK_17A57A04BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE compagny_image ADD CONSTRAINT FK_42A0E791224ABE0 FOREIGN KEY (compagny_id) REFERENCES compagny (id)');
        $this->addSql('ALTER TABLE compagny_internship ADD CONSTRAINT FK_FA65D9361224ABE0 FOREIGN KEY (compagny_id) REFERENCES compagny (id)');
        $this->addSql('ALTER TABLE degree ADD CONSTRAINT FK_A7A36D63C32A47EE FOREIGN KEY (school_id) REFERENCES school (id)');
        $this->addSql('ALTER TABLE internship_period ADD CONSTRAINT FK_56D813A36C682D0 FOREIGN KEY (trainee_id) REFERENCES trainee (id)');
        $this->addSql('ALTER TABLE school ADD CONSTRAINT FK_F99EDABBBF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE school_image ADD CONSTRAINT FK_1BC7B245C32A47EE FOREIGN KEY (school_id) REFERENCES school (id)');
        $this->addSql('ALTER TABLE trainee ADD CONSTRAINT FK_46C68DE7C32A47EE FOREIGN KEY (school_id) REFERENCES school (id)');
        $this->addSql('ALTER TABLE trainee ADD CONSTRAINT FK_46C68DE7BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compagny_image DROP FOREIGN KEY FK_42A0E791224ABE0');
        $this->addSql('ALTER TABLE compagny_internship DROP FOREIGN KEY FK_FA65D9361224ABE0');
        $this->addSql('ALTER TABLE degree DROP FOREIGN KEY FK_A7A36D63C32A47EE');
        $this->addSql('ALTER TABLE school_image DROP FOREIGN KEY FK_1BC7B245C32A47EE');
        $this->addSql('ALTER TABLE trainee DROP FOREIGN KEY FK_46C68DE7C32A47EE');
        $this->addSql('ALTER TABLE internship_period DROP FOREIGN KEY FK_56D813A36C682D0');
        $this->addSql('ALTER TABLE compagny DROP FOREIGN KEY FK_17A57A04BF396750');
        $this->addSql('ALTER TABLE school DROP FOREIGN KEY FK_F99EDABBBF396750');
        $this->addSql('ALTER TABLE trainee DROP FOREIGN KEY FK_46C68DE7BF396750');
        $this->addSql('DROP TABLE compagny');
        $this->addSql('DROP TABLE compagny_image');
        $this->addSql('DROP TABLE compagny_internship');
        $this->addSql('DROP TABLE degree');
        $this->addSql('DROP TABLE internship_period');
        $this->addSql('DROP TABLE school');
        $this->addSql('DROP TABLE school_image');
        $this->addSql('DROP TABLE trainee');
        $this->addSql('DROP TABLE user');
    }
}
