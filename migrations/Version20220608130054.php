<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220608130054 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compagny CHANGE contact_phone contact_phone VARCHAR(35) NOT NULL COMMENT \'(DC2Type:phone_number)\'');
        $this->addSql('ALTER TABLE school CHANGE contact_phone contact_phone VARCHAR(35) NOT NULL COMMENT \'(DC2Type:phone_number)\'');
        $this->addSql('ALTER TABLE setting CHANGE value value LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('ALTER TABLE trainee CHANGE phone phone VARCHAR(35) NOT NULL COMMENT \'(DC2Type:phone_number)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compagny CHANGE contact_phone contact_phone VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE school CHANGE contact_phone contact_phone VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE setting CHANGE value value TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE trainee CHANGE phone phone VARCHAR(255) NOT NULL');
    }
}
