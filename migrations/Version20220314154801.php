<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220314154801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE trainee ADD degree_id INT NOT NULL');
        $this->addSql('ALTER TABLE trainee ADD CONSTRAINT FK_46C68DE7B35C5756 FOREIGN KEY (degree_id) REFERENCES degree (id)');
        $this->addSql('CREATE INDEX IDX_46C68DE7B35C5756 ON trainee (degree_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE trainee DROP FOREIGN KEY FK_46C68DE7B35C5756');
        $this->addSql('DROP INDEX IDX_46C68DE7B35C5756 ON trainee');
        $this->addSql('ALTER TABLE trainee DROP degree_id');
    }
}
