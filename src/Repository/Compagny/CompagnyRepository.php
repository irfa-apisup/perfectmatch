<?php

namespace App\Repository\Compagny;

use App\Entity\Compagny\Compagny;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Compagny|null find($id, $lockMode = null, $lockVersion = null)
 * @method Compagny|null findOneBy(array $criteria, array $orderBy = null)
 * @method Compagny[]    findAll()
 * @method Compagny[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompagnyRepository extends ServiceEntityRepository
{
    private const DAYS_BEFORE_NOTIFY = 1;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Compagny::class);
    }

    /**
     * @return Compagny[] Returns an array of Compagny objects
     */
    public function findCompagnyOrderActive($limit = null)
    {
        $query = $this->createQueryBuilder('c')
            ->select('c')
            ->orderBy('c.active', 'ASC')
            ->setMaxResults($limit)
            ->getQuery();
        return $query->getResult();
    }

    public function countActive()
    {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->where('c.active = true')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countInactive()
    {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->where('c.active = false')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function searchByFilter($name = null)
    {
        $query = $this->createQueryBuilder('c');

        if ($this->isValid($name)) {
            $query = $query
                ->andWhere("c.name LIKE '%$name%'");
        }

        return $query->getQuery()->getResult();
    }

    private function isValid($field): bool
    {
        if (!empty($field) && !is_null($field)) {
            return true;
        }

        return false;
    }

    public function countNewDailyRegistration()
    {
        $qb = $this->createQueryBuilder('c');

        return $qb
            ->select('COUNT(c)')
            ->where($qb->expr()->abs('DATE_DIFF(c.created_at, CURRENT_DATE())') . ' = :days')
            ->andWhere('c.active = false')
            ->setParameter(':days', self::DAYS_BEFORE_NOTIFY)
            ->getQuery()
            ->getSingleScalarResult();
    }

    // public function findAllInternshipData($id)
    // {
    //     return $this->createQueryBuilder('c')
    //         ->select("c.id, c.name, c.contactFirstname, c.contactLastname, c.contactMail, i.title, i.contractType, i.vacancies")
    //         ->innerJoin(CompagnyInternship::class, 'i', Join::WITH, 'c.id = i.compagny')
    //         ->where('c.id = :id')
    //         ->getQuery()
    //         ->getResult();
    // }

    // public function findAllCompagnyByUser($userId)
    // {
    //     return $this->createQueryBuilder('c')
    //         ->innerJoin('compagny_compagny_user', 'r')
    //         ->where('c.owner = :id')
    //         ->andWhere('r.compagny_user_id = :id')
    //         ->setParameter(':id', $userId)
    //         ->getQuery()
    //         ->getResult();
    // }

    // /**
    //  * @return Compagny[] Returns an array of Compagny objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Compagny
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
