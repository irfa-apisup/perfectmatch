<?php

namespace App\Repository\Compagny;

use App\Entity\Compagny\CompagnyInternship;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use function dump;

/**
 * @method CompagnyInternship|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompagnyInternship|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompagnyInternship[]    findAll()
 * @method CompagnyInternship[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompagnyInternshipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompagnyInternship::class);
    }

    public function searchByFilter($title = null, $startPeriod = null, $endPeriod = null)
    {
        $query = $this->createQueryBuilder('i');

        if ($this->isValid($title)) {
            $query = $query
                ->andWhere("i.title LIKE '%$title%'");
        }

        if ($this->isValid($startPeriod) && $this->isValid($endPeriod)) {
            $start = date_create($startPeriod);
            $end = date_create($endPeriod);


            if ($start instanceof DateTime && $end instanceof DateTime) {
                $query = $query
                    ->andWhere('i.startPeriod >= :start')
                    ->andWhere('i.endPeriod <= :end')
                    ->setParameter(':start', $start)
                    ->setParameter(':end', $end);
            }
        }

        return $query->getQuery()->getResult();
    }

    private function isValid($field): bool
    {
        if (!empty($field) && !is_null($field)) {
            return true;
        }

        return false;
    }

    // /**
    //  * @return CompagnyInternship[] Returns an array of Internship objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompagnyInternship
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
