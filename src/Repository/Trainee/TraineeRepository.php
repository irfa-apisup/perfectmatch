<?php

namespace App\Repository\Trainee;

use App\Entity\School\Degree;
use App\Entity\Trainee\Trainee;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use function dump;

/**
 * @method Trainee|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trainee|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trainee[]    findAll()
 * @method Trainee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TraineeRepository extends ServiceEntityRepository
{
    private const DAYS_BEFORE_REJECTED_REMOVAL = 365;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trainee::class);
    }

    public function findTraineeOrderActive($limit = null)
    {
        $query = $this->createQueryBuilder('t')
            ->select('t')
            ->orderBy('t.active', 'ASC')
            ->setMaxResults($limit)
            ->getQuery();
        return $query->getResult();
    }

    public function searchByFilter($job = null, $zone = null, $startPeriod = null, $endPeriod = null)
    {
        $query = $this->createQueryBuilder('t');

        if ($this->isValid($job)) {
            $count = 0;
            $query = $query
                ->leftJoin(
                Degree::class,
                'd',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                't MEMBER OF d.trainees'
            );

            foreach ($job as $j){
                $count++;
                $query = $query
                    ->andWhere("d.job = :job$count")
                    ->setParameter("job$count", $j);
            }
        }

        if ($this->isValid($zone)) {
            $count = 0;
            foreach ($zone as $z) {
                $count++;
                $query = $query
                    ->andWhere($query->expr()->like('t.zone', ":zone$count"))
                    ->setParameter("zone$count", '%' . $z . '%');
            }
        }

        if ($this->isValid($startPeriod) && $this->isValid($endPeriod)) {
            $start = date_create($startPeriod);
            $end = date_create($endPeriod);
            if ($start instanceof DateTime && $end instanceof DateTime) {
                $query = $query
                    ->andWhere('p.start >= :start')
                    ->andWhere('p.End <= :end')
                    ->innerJoin('t.Period', 'p')
                    ->setParameter(':start', $start)
                    ->setParameter(':end', $end);
            }
        }
        return $query->getQuery()->getResult();
    }

    public function disableInactive()
    {
        $qb = $this->createQueryBuilder('t');

        return $qb
            ->update()
            ->set('t.initialized', 'false')
            ->where($qb->expr()->abs('DATE_DIFF(t.updated_at, CURRENT_DATE())') . ' >= :days')
            ->andWhere('t.initialized = true')
            ->setParameter(':days', self::DAYS_BEFORE_REJECTED_REMOVAL)
            ->getQuery()
            ->getResult();
    }

    public function countInactive()
    {
        $qb = $this->createQueryBuilder('t');

        return $qb
            ->select('COUNT(t)')
            ->where($qb->expr()->abs('DATE_DIFF(t.updated_at, CURRENT_DATE())') . ' >= :days')
            ->andWhere('t.initialized = true')
            ->setParameter(':days', self::DAYS_BEFORE_REJECTED_REMOVAL)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countActive()
    {
        return $this->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->where('t.active = true')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countOnlyInactive()
    {
        return $this->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->where('t.active = false')
            ->getQuery()
            ->getSingleScalarResult();
    }

    // /**
    //  * @return Trainee[] Returns an array of Trainee objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Trainee
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    private function isValid($field): bool
    {
        if (!empty($field) && !is_null($field)) {
            return true;
        }

        return false;
    }
}
