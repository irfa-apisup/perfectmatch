<?php

namespace App\Repository\Trainee;

use App\Entity\InternshipPeriod;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InternshipPeriod|null find($id, $lockMode = null, $lockVersion = null)
 * @method InternshipPeriod|null findOneBy(array $criteria, array $orderBy = null)
 * @method InternshipPeriod[]    findAll()
 * @method InternshipPeriod[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InternshipPeriodRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InternshipPeriod::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(InternshipPeriod $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(InternshipPeriod $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return InternshipPeriod[] Returns an array of InternshipPeriod objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InternshipPeriod
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
