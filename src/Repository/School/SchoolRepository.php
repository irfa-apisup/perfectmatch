<?php

namespace App\Repository\School;

use App\Entity\School\School;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method School|null find($id, $lockMode = null, $lockVersion = null)
 * @method School|null findOneBy(array $criteria, array $orderBy = null)
 * @method School[]    findAll()
 * @method School[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SchoolRepository extends ServiceEntityRepository
{

    private const DAYS_BEFORE_NOTIFY = 1; // 0 => Notify today

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, School::class);
    }

    public function findOneByRoleThatSucksLess(string $role)
    {
        $role = mb_strtoupper($role);

        return $this->createQueryBuilder('u')
            ->andWhere('JSON_CONTAINS(u.roles, :role) = 1')
            ->setParameter('role', '"ROLE_' . $role . '"')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return School[] Returns an array of School objects
     */
    public function findSchoolOrderActive()
    {
        return $this->createQueryBuilder('s')
            ->where('JSON_CONTAINS(s.roles, :role) = 0')
            ->setParameter('role', '"ROLE_ADMIN"')
            ->orderBy('s.active', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function countActive()
    {
        return $this->createQueryBuilder('s')
            ->select('COUNT(s.id)')
            ->where('s.active = true')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countInactive()
    {
        return $this->createQueryBuilder('s')
            ->select('COUNT(s.id)')
            ->where('s.active = false')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function searchByFilter($name = null)
    {
        $query = $this->createQueryBuilder('s');

        if ($this->isValid($name)) {
            $query = $query
                ->andWhere("s.name LIKE '%$name%'");
        }

        return $query->getQuery()->getResult();
    }

    private function isValid($field): bool
    {
        if (!empty($field) && !is_null($field)) {
            return true;
        }

        return false;
    }

    public function countNewDailyRegistration()
    {
        $qb = $this->createQueryBuilder('s');

        return $qb
            ->select('COUNT(s)')
            ->where($qb->expr()->abs('DATE_DIFF(s.created_at, CURRENT_DATE())') . ' = :days')
            ->andWhere('s.active = false')
            ->setParameter(':days', self::DAYS_BEFORE_NOTIFY)
            ->getQuery()
            ->getSingleScalarResult();
    }


    // /**
    //  * @return School[] Returns an array of School objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?School
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
