<?php

namespace App\Repository\School;

use App\Entity\School\Degree;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Degree|null find($id, $lockMode = null, $lockVersion = null)
 * @method Degree|null findOneBy(array $criteria, array $orderBy = null)
 * @method Degree[]    findAll()
 * @method Degree[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DegreeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Degree::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Degree $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Degree $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function countActive()
    {
        return $this->createQueryBuilder('d')
            ->select('COUNT(d.id)')
            ->where('d.active = true')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countInactive()
    {
        return $this->createQueryBuilder('d')
            ->select('COUNT(d.id)')
            ->where('d.active = false')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findAllJob()
    {
        return $this->createQueryBuilder('d')
            ->select('d.job')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Degree[] Returns an array of Degree objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Degree
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
