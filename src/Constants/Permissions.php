<?php

namespace App\Constants;

abstract class Permissions
{
    // Roles
    // Add Individual permissions to role here
    public const ROLE_ADMIN = [...Permissions::ROLE_USER, Permissions::ROLE_ADMIN_NAME, self::ROLE_SCHOOL_NAME];
    public const ROLE_COMPAGNY = [...Permissions::ROLE_USER, Permissions::ROLE_COMPAGNY_NAME];
    public const ROLE_SCHOOL = [...Permissions::ROLE_USER, Permissions::ROLE_SCHOOL_NAME];
    public const ROLE_TRAINEE = [...Permissions::ROLE_USER, Permissions::ROLE_TRAINEE_NAME];
    public const ROLE_USER = ['ROLE_USER'];

    // Individual permission

    // Roles
    public const ROLE_ADMIN_NAME = 'ROLE_ADMIN';
    public const ROLE_COMPAGNY_NAME = 'ROLE_COMPAGNY';
    public const ROLE_SCHOOL_NAME = 'ROLE_SCHOOL';
    public const ROLE_TRAINEE_NAME = 'ROLE_TRAINEE';
}
