<?php

namespace App\Constants;

use App\Entity\Setting;
use App\Repository\SettingRepository;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

use function PHPUnit\Framework\isNull;

class Settings
{
    private SettingRepository $settingsRepository;
    public $Anonimisation;

    public function __construct(SettingRepository $settingRepository)
    {
        $this->settingsRepository = $settingRepository;
        $this->Anonimisation = (new Setting())->setName('anonimisation')->setValue(false)->setDescription('');
    }

    /**
     * Get the value of Anonimisation.
     */
    public function getAnonimisation()
    {
        return $this->Anonimisation;
    }

    /**
     * Get the value of Anonimisation.
     */
    public function isAnonymisationEnabled(): bool
    {
        /**
         * @var Setting $setting
         */
        $setting = $this->settingsRepository->findOneBy(['name' => $this->Anonimisation->getName()]);
        if($setting === null){return false;}
        return $setting->getValue();
    }
}
