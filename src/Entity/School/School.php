<?php

namespace App\Entity\School;

use App\Entity\Trainee\Trainee;
use App\Entity\User;
use App\Repository\School\SchoolRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use libphonenumber\PhoneNumber;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=SchoolRepository::class)
 * @Vich\Uploadable()
 */
class School extends User
{
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 2,
     *      max = 25,
     *      minMessage = "Your school name must be at least {{ limit }} characters long",
     *      maxMessage = "Your school name cannot be longer than {{ limit }} characters"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = 2,
     *      max = 25,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private $contactFirstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = 2,
     *      max = 25,
     *      minMessage = "Your last name must be at least {{ limit }} characters long",
     *      maxMessage = "Your last name cannot be longer than {{ limit }} characters"
     * )
     */
    private $contactLastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     */
    private $contactMail;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *      pattern="/[0-9]+\s*([a-zA-Z]+\s*[a-zA-Z]+\s)*[0-9]* /",
     *      message="Your address format is invalid"
     * )
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *      pattern="/^[a-zA-Z0-9]{0,10}$/",
     *      message="Your postalcode format is invalid"
     * )
     */
    private $postalcode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="phone_number")
     */
    private $contactPhone;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;


    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex(
     *      pattern="/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)/",
     *      message="Your website is invalid"
     * )
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logofilename;

    /**
     * @Assert\Image(
     *  mimeTypes={"image/jpeg","image/jpg","image/png","image/webp","image/tiff"}
     * )
     * @Vich\UploadableField(mapping="logo", fileNameProperty="logofilename")
     */
    private $logoFile;

    /**
     * @ORM\OneToMany(targetEntity=SchoolImage::class, mappedBy="school", cascade={"persist"}, orphanRemoval=true)
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity=Trainee::class, mappedBy="school")
     */
    private $trainees;

    /**
     * @ORM\OneToMany(targetEntity=Degree::class, mappedBy="school", orphanRemoval=true)
     */
    private $Degrees;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $organisationtype;

    public function __construct()
    {
        $this->trainee = new ArrayCollection();
        $this->internships = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->trainees = new ArrayCollection();
        $this->Degrees = new ArrayCollection();
    }

    public function getGlobalName(): string{
        return $this->name;
    }


    public function __toString()
    {
        return $this->name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContactFirstname(): ?string
    {
        return $this->contactFirstname;
    }

    public function setContactFirstname(?string $contactFirstname): self
    {
        $this->contactFirstname = $contactFirstname;

        return $this;
    }

    public function getContactLastname(): ?string
    {
        return $this->contactLastname;
    }

    public function setContactLastname(string $contactLastname): self
    {
        $this->contactLastname = $contactLastname;

        return $this;
    }

    public function getContactMail(): ?string
    {
        return $this->contactMail;
    }

    public function setContactMail(?string $contactMail): self
    {
        $this->contactMail = $contactMail;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalcode(): ?string
    {
        return $this->postalcode;
    }

    public function setPostalcode(string $postalcode): self
    {
        $this->postalcode = $postalcode;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getContactPhone(): ?PhoneNumber
    {
        return $this->contactPhone;
    }

    public function setContactPhone(PhoneNumber $contactPhone): self
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    /**
     * @return Collection<int, SchoolImage>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(SchoolImage $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setSchool($this);
        }

        return $this;
    }

    public function removeImage(SchoolImage $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getSchool() === $this) {
                $image->setSchool(null);
            }
        }

        return $this;
    }

    public function getLogoFilename(): ?string
    {
        return $this->logofilename;
    }

    public function setLogoFilename(?string $logofilename): self
    {
        $this->logofilename = $logofilename;

        return $this;
    }

    public function getLogoFile(): ?File
    {
        return $this->logoFile;
    }

    public function setLogoFile(?File $logoFile): self
    {
        $this->logoFile = $logoFile;

        if ($this->logoFile instanceof UploadedFile) {
            $this->updated_at = new DateTime('now');
        }

        return $this;
    }

    /**
     * @return Collection<int, Trainee>
     */
    public function getTrainees(): Collection
    {
        return $this->trainees;
    }

    public function addTrainee(Trainee $trainee): self
    {
        if (!$this->trainees->contains($trainee)) {
            $this->trainees[] = $trainee;
            $trainee->setSchool($this);
        }

        return $this;
    }

    public function removeTrainee(Trainee $trainee): self
    {
        if ($this->trainees->removeElement($trainee)) {
            // set the owning side to null (unless already changed)
            if ($trainee->getSchool() === $this) {
                $trainee->setSchool(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Degree>
     */
    public function getDegrees(): Collection
    {
        return $this->Degrees;
    }

    public function addDegree(Degree $degree): self
    {
        if (!$this->Degrees->contains($degree)) {
            $this->Degrees[] = $degree;
            $degree->setSchool($this);
        }

        return $this;
    }

    public function removeDegree(Degree $degree): self
    {
        if ($this->Degrees->removeElement($degree)) {
            // set the owning side to null (unless already changed)
            if ($degree->getSchool() === $this) {
                $degree->setSchool(null);
            }
        }

        return $this;
    }

    public function getOrganisationtype(): ?string
    {
        return $this->organisationtype;
    }

    public function setOrganisationtype(string $organisationtype): self
    {
        $this->organisationtype = $organisationtype;

        return $this;
    }
}
