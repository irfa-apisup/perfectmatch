<?php

namespace App\Entity;

use App\Entity\Compagny\Compagny;
use App\Entity\Trainee\Trainee;
use App\Repository\ReportRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 */
class Report
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity=Compagny::class, inversedBy="reports")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity=Trainee::class, inversedBy="reports")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Trainee;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getCompany(): ?Compagny
    {
        return $this->company;
    }

    public function setCompany(?Compagny $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getTrainee(): ?Trainee
    {
        return $this->Trainee;
    }

    public function setTrainee(?Trainee $Trainee): self
    {
        $this->Trainee = $Trainee;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
