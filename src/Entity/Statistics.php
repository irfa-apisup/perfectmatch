<?php

namespace App\Entity;

use App\Repository\StatisticsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StatisticsRepository::class)
 */
class Statistics
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $traineeProfil = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $internship = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $traineeContact = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $compagnyContact = 0;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="statistics", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\Column(type="integer")
     */
    private $request = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTraineeProfil(): ?int
    {
        return $this->traineeProfil;
    }

    public function setTraineeProfil(int $traineeProfil): self
    {
        $this->traineeProfil = $traineeProfil;

        return $this;
    }

    public function increaseTraineeProfil(): self
    {
        $this->traineeProfil = $this->traineeProfil + 1;

        return $this;
    }

    public function getTraineeContact(): ?int
    {
        return $this->traineeContact;
    }

    public function setTraineeContact(int $traineeContact): self
    {
        $this->traineeContact = $traineeContact;

        return $this;
    }

    public function increaseTraineeContact(): self
    {
        $this->traineeContact = $this->traineeContact + 1;

        return $this;
    }

    public function getCompagnyContact(): ?int
    {
        return $this->compagnyContact;
    }

    public function setCompagnyContact(int $compagnyContact): self
    {
        $this->compagnyContact = $compagnyContact;

        return $this;
    }

    public function increaseCompagnyContact(): self
    {
        $this->compagnyContact = $this->compagnyContact + 1;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get the value of internship.
     */
    public function getInternship()
    {
        return $this->internship;
    }

    /**
     * Set the value of internship.
     */
    public function setInternship($internship): self
    {
        $this->internship = $internship;

        return $this;
    }

    public function increaseInternship(): self
    {
        $this->internship = $this->internship + 1;

        return $this;
    }

    public function getRequest(): ?int
    {
        return $this->request;
    }

    public function setRequest(int $request): self
    {
        $this->request = $request;

        return $this;
    }

    public function increaseRequest(): self
    {
        $this->request = $this->request + 1;

        return $this;
    }
}
