<?php

namespace App\Entity\Compagny;

use App\Repository\Compagny\CompagnyInternshipRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=CompagnyInternshipRepository::class)
 * @Vich\Uploadable()
 */
class CompagnyInternship
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity=Compagny::class, inversedBy="internships")
     * @ORM\JoinColumn(nullable=false)
     */
    private $compagny;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $workingTime;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @ORM\Column(type="integer")
     */
    private $vacancies;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     * * @Assert\Length(
     *      min = 2,
     *      max = 1500,
     * )
     */
    private $searchProfil;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startPeriod;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endPeriod;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $prerequisite;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $another;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $filename;

    private $originalname;

    /**
     * @Assert\File(
     *  mimeTypes="application/pdf",
     *  maxSize = "40M",
     * )
     * @Vich\UploadableField(mapping="internship_documents", fileNameProperty="filename")
     * @var File|null
     */
    private $file;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    public function __construct()
    {
        $this->applicant = new ArrayCollection();
        $this->acceptIntern = new ArrayCollection();
        $this->declinedIntern = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCompagny(): ?Compagny
    {
        return $this->compagny;
    }

    public function setCompagny(?Compagny $compagny): self
    {
        $this->compagny = $compagny;

        return $this;
    }

    public function getWorkingTime(): ?string
    {
        return $this->workingTime;
    }

    public function setWorkingTime(?string $workingTime): self
    {
        $this->workingTime = $workingTime;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getVacancies(): ?string
    {
        return $this->vacancies;
    }

    public function setVacancies(?string $vacancies): self
    {
        $this->vacancies = $vacancies;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSearchProfil(): ?string
    {
        return $this->searchProfil;
    }

    public function setSearchProfil(?string $searchProfil): self
    {
        $this->searchProfil = $searchProfil;

        return $this;
    }

    public function getStartPeriod(): ?DateTimeInterface
    {
        return $this->startPeriod;
    }

    public function setStartPeriod($startPeriod): self
    {
        if ($startPeriod instanceof DateTimeInterface) {
            $this->startPeriod = $startPeriod;
        } elseif (is_string($startPeriod)) {
            $this->startPeriod = date_create($startPeriod);
        }

        return $this;
    }

    public function getEndPeriod(): ?DateTimeInterface
    {
        return $this->endPeriod;
    }

    public function setEndPeriod($endPeriod): self
    {
        if ($endPeriod instanceof DateTimeInterface) {
            $this->endPeriod = $endPeriod;
        } elseif (is_string($endPeriod)) {
            $this->endPeriod = date_create($endPeriod);
        }

        return $this;
    }

    public function getPrerequisite(): ?string
    {
        return $this->prerequisite;
    }

    public function setPrerequisite(?string $prerequisite): self
    {
        $this->prerequisite = $prerequisite;

        return $this;
    }

    public function getAnother(): ?string
    {
        return $this->another;
    }

    public function setAnother(?string $another): self
    {
        $this->another = $another;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function getOriginalName(): ?string
    {
        $split = explode("-62c", $this->filename);
        $ext = explode('.', $split[1])[1];
        $this->originalname = "$split[0].$ext";
        return $this->originalname;
    }

    public function setFilename(?string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getFile(): ?File
    {
        if($this->filename !== null && $this->filename != ""){
            $split = explode("-62c", $this->filename);
            $ext = explode('.', $split[1])[1];
            $this->originalname = "$split[0].$ext";
        }
        return $this->file;
    }

    public function setFile(?File $file): self
    {
        $this->file = $file;
        if ($this->file instanceof UploadedFile) {
            $this->updated_at = new \DateTime('now');
        }

        return $this;
    }

    public function setUpdatedAt(DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updated_at;
    }
}
