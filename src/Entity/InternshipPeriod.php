<?php

namespace App\Entity;

use App\Entity\Trainee\Trainee;
use App\Repository\Trainee\InternshipPeriodRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InternshipPeriodRepository::class)
 */
class InternshipPeriod
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $start;

    /**
     * @ORM\Column(type="datetime")
     */
    private $End;

    /**
     * @ORM\ManyToOne(targetEntity=Trainee::class, inversedBy="Period")
     * @ORM\JoinColumn(nullable=false)
     */
    private $trainee;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStart(): ?DateTimeInterface
    {
        return $this->start;
    }

    public function setStart($start): self
    {
        if ($start instanceof DateTimeInterface) {
            $this->start = $start;
        } elseif (is_string($start)) {
            $this->start = date_create($start);
        }

        return $this;
    }

    public function getEnd(): ?DateTimeInterface
    {
        return $this->End;
    }

    public function setEnd($End): self
    {
        if ($End instanceof DateTimeInterface) {
            $this->End = $End;
        } elseif (is_string($End)) {
            $this->End = date_create($End);
        }

        return $this;
    }

    public function getTrainee(): ?Trainee
    {
        return $this->trainee;
    }

    public function setTrainee(?Trainee $trainee): self
    {
        $this->trainee = $trainee;

        return $this;
    }
}
