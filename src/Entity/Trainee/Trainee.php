<?php

namespace App\Entity\Trainee;

use App\Entity\Document;
use App\Entity\InternshipPeriod;
use App\Entity\Report;
use App\Entity\School\Degree;
use App\Entity\School\School;
use App\Entity\User;
use App\Repository\Trainee\TraineeRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=TraineeRepository::class)
 * @Vich\Uploadable()
 */
class Trainee extends User
{
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 2,
     *      max = 25,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 2,
     *      max = 25,
     *      minMessage = "Your last name must be at least {{ limit }} characters long",
     *      maxMessage = "Your last name cannot be longer than {{ limit }} characters"
     * )
     */
    private $lastname;

    /**
     * @ORM\Column(type="phone_number")
     */
    private $phone;

    /**
     * @ORM\ManyToOne(targetEntity=School::class, inversedBy="trainees")
     * @ORM\JoinColumn(nullable=false)
     */
    private $school;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex(
     *      pattern="/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)/",
     *      message="Your website is invalid"
     * )
     */
    private $website;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $other;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex(
     *      pattern="/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)/",
     *      message="Your website is invalid"
     * )
     */
    private $link;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $objectif;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Assert\Choice(
     *     choices = {"Regional", "National", "International"},
     *     message = "Choose a valid zone.",
     *     multiple= true
     * )
     */
    private $zone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avatarfilename;

    /**
     * @Assert\Image(
     *  mimeTypes={"image/jpeg","image/jpg","image/png","image/webp","image/tiff"}
     * )
     * @Vich\UploadableField(mapping="trainee_avatar", fileNameProperty="avatarfilename")
     */
    private $avatarFile;

    /**
     * @ORM\OneToMany(targetEntity=InternshipPeriod::class, mappedBy="trainee", cascade={"persist"}, orphanRemoval=true)
     */
    private $Period;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $languages;

    /**
     * @ORM\ManyToOne(targetEntity=Degree::class, inversedBy="trainees")
     * @ORM\JoinColumn(nullable=false)
     */
    private $degree;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="boolean")
     */
    private $initialized;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="trainee", cascade={"persist"}, orphanRemoval=true)
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity=Report::class, mappedBy="Trainee")
     */
    private $reports;

    public function __construct()
    {
        parent::__construct();
        $this->Period = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->reports = new ArrayCollection();
    }

    public function getGlobalName(): string{
        return $this->firstname;
    }


    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPhone(): ?PhoneNumber
    {
        return $this->phone;
    }

    public function setPhone(PhoneNumber $phone): PhoneNumber
    {
        $this->phone = $phone;

        return $this->phone;
    }

    public function getSchool(): ?School
    {
        return $this->school;
    }

    public function setSchool(?School $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOther(): ?string
    {
        return $this->other;
    }

    public function setOther(?string $other): self
    {
        $this->other = $other;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getObjectif(): ?string
    {
        return $this->objectif;
    }

    public function setObjectif(?string $objectif): self
    {
        $this->objectif = $objectif;

        return $this;
    }

    public function getZone(): ?array
    {
        return $this->zone;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function setZone(?array $zone): self
    {
        $this->zone = $zone;

        return $this;
    }

    public function getAvatarFilename(): ?string
    {
        return $this->avatarfilename;
    }

    public function setAvatarFilename(?string $avatarfilename): self
    {
        $this->avatarfilename = $avatarfilename;

        return $this;
    }

    public function getAvatarFile(): ?File
    {
        return $this->avatarFile;
    }

    public function setAvatarFile(?File $avatarFile): self
    {
        $this->avatarFile = $avatarFile;

        if ($this->avatarFile instanceof UploadedFile) {
            $this->updated_at = new DateTime('now');
        }

        return $this;
    }

    /**
     * @return Collection<int, InternshipPeriod>
     */
    public function getPeriod(): Collection
    {
        return $this->Period;
    }

    public function addPeriod(InternshipPeriod $period): self
    {
        if (!$this->Period->contains($period)) {
            $this->Period[] = $period;
            $period->setTrainee($this);
        }

        return $this;
    }

    public function removePeriod(InternshipPeriod $period): self
    {
        if ($this->Period->removeElement($period)) {
            // set the owning side to null (unless already changed)
            if ($period->getTrainee() === $this) {
                $period->setTrainee(null);
            }
        }

        return $this;
    }

    public function getLanguages(): ?array
    {
        return json_decode($this->languages) ?? [];
    }

    public function setLanguages(?array $languages): self
    {
        $this->languages = json_encode($languages) ?? null;

        return $this;
    }

    public function getDegree(): ?Degree
    {
        return $this->degree;
    }

    public function setDegree(?Degree $degree): self
    {
        $this->degree = $degree;

        return $this;
    }

    public function getInitialized(): ?bool
    {
        return $this->initialized;
    }

    public function setInitialized(bool $initialized): self
    {
        $this->initialized = $initialized;

        return $this;
    }

    /**
     * @return Collection<int, Document>
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setTrainee($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getTrainee() === $this) {
                $document->setTrainee(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Report>
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setTrainee($this);
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getTrainee() === $this) {
                $report->setTrainee(null);
            }
        }

        return $this;
    }
}
