<?php

namespace App\Entity;

use App\Repository\SurveyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SurveyRepository::class)
 */
class Survey
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quality;

    /**
     * @ORM\Column(type="integer")
     */
    private $Satisfaction;

    /**
     * @ORM\Column(type="integer")
     */
    private $Facility;

    /**
     * @ORM\Column(type="integer")
     */
    private $Relevance;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Improvement;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="surveys")
     */
    private $owner;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuality(): ?int
    {
        return $this->quality;
    }

    public function setQuality(int $quality): self
    {
        $this->quality = $quality;

        return $this;
    }

    public function getSatisfaction(): ?int
    {
        return $this->Satisfaction;
    }

    public function setSatisfaction(int $Satisfaction): self
    {
        $this->Satisfaction = $Satisfaction;

        return $this;
    }

    public function getFacility(): ?int
    {
        return $this->Facility;
    }

    public function setFacility(int $Facility): self
    {
        $this->Facility = $Facility;

        return $this;
    }

    public function getRelevance(): ?int
    {
        return $this->Relevance;
    }

    public function setRelevance(int $Relevance): self
    {
        $this->Relevance = $Relevance;

        return $this;
    }

    public function getImprovement(): ?string
    {
        return $this->Improvement;
    }

    public function setImprovement(?string $Improvement): self
    {
        $this->Improvement = $Improvement;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }
}
