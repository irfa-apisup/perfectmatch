<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Contact
{
    /**
     * @Assert\Length(min=2,max=100)
     *
     * @var string|null
     */
    private $name;

    /**
     * @Assert\Email()
     *
     * @var string
     */
    private $recipient;

    /**
     * @Assert\Length(min=2,max=100)
     *
     * @var string
     */
    private $firstname;

    /**
     * @Assert\Length(min=2,max=100)
     *
     * @var string
     */
    private $lastname;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(
     *      pattern="/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/",
     *      message="Your phone format is invalid"
     * )
     *
     * @var string
     */
    private $phone;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     *
     * @var string
     */
    private $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=10)
     *
     * @var string
     */
    private $message;

    /**
     * Get the value of message.
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set the value of message.
     */
    public function setMessage($message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get the value of email.
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email.
     */
    public function setEmail($email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of phone.
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the value of phone.
     */
    public function setPhone($phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get the value of lastname.
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set the value of lastname.
     */
    public function setLastname($lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get the value of firstname.
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set the value of firstname.
     */
    public function setFirstname($firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get the value of name.
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name.
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of recipient.
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set the value of recipient.
     */
    public function setRecipient($recipient): self
    {
        $this->recipient = $recipient;

        return $this;
    }
}
