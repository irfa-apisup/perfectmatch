<?php

namespace App\Controller;

use App\Constants\Permissions;
use App\Entity\Compagny\Compagny;
use App\Entity\School\School;
use App\Entity\Statistics;
use App\Entity\Trainee\Trainee;
use App\Form\Compagny\CompagnyFlow;
use App\Form\School\SchoolFlow;
use App\Form\Trainee\TraineeFlow;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Register trainee account.
     *
     * @param UserPasswordHasherInterface $hash
     * @param Request                     $request
     *
     * @return Response
     */
    #[Route('/register/trainee', name: 'register_trainee')]
    public function registerTrainee(UserPasswordHasherInterface $hash, TraineeFlow $flow): Response
    {
        if (null != $this->getUser()) {
            return $this->redirectToRoute('account');
        }

        $trainee = new Trainee();
        $flow->bind($trainee);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();
            } else {
                $password = $hash->hashPassword($trainee, $trainee->getPassword());
                $trainee->setPassword($password);
                $trainee->setActive(false);
                $trainee->setInitialized(true);
                $trainee->setRoles([Permissions::ROLE_TRAINEE_NAME]);
                $trainee->setUpdatedAt(new DateTime('now'));
                $statistics = new Statistics();
                $statistics->setOwner($trainee);
                $this->entityManager->persist($trainee);
                $this->entityManager->persist($statistics);
                $this->entityManager->flush();



                $flow->reset(); // remove step data from the session

                return $this->redirectToRoute('app_login'); // redirect when done
            }
        }

        return $this->render('register/index.html.twig', [
            'form' => $form->createView(),
            'flow' => $flow,
        ]);
    }

    /**
     * Register all other account (Admin ,Compagny, School).
     *
     * @param UserPasswordHasherInterface $hash
     * @param Request                     $request
     *
     * @return Response
     */
    #[Route('/register/compagny', name: 'register_compagny')]
    public function registerCompagny(UserPasswordHasherInterface $hash, Request $request, CompagnyFlow $flow): Response
    {
        if (null != $this->getUser()) {
            return $this->redirectToRoute('account');
        }

        $compagny = new Compagny();
        $flow->bind($compagny);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();
            } else {
                $now = new DateTime('now');
                $password = $hash->hashPassword($compagny, $compagny->getPassword());
                $compagny->setPassword($password);
                $compagny->setActive(false);
                $compagny->setCreatedAt($now);
                $compagny->setUpdatedAt($now);
                $compagny->setRoles([Permissions::ROLE_COMPAGNY_NAME]);
                $statistics = new Statistics();
                $statistics->setOwner($compagny);
                $this->entityManager->persist($compagny);
                $this->entityManager->persist($statistics);
                $this->entityManager->flush();

                $flow->reset(); // remove step data from the session

                return $this->redirectToRoute('app_login'); // redirect when done
            }
        }

        return $this->render('register/index.html.twig', [
            'form' => $form->createView(),
            'flow' => $flow,
        ]);
    }

    /**
     * Register school account.
     *
     * @param UserPasswordHasherInterface $hash
     * @param Request                     $request
     *
     * @return Response
     */
    #[Route('/register/school', name: 'register_school')]
    public function registerSchool(UserPasswordHasherInterface $hash, SchoolFlow $flow): Response
    {
        if (null != $this->getUser()) {
            return $this->redirectToRoute('account');
        }

        $school = new School();
        $flow->bind($school);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();
            } else {
                $now = new DateTime('now');
                $password = $hash->hashPassword($school, $school->getPassword());
                $school->setPassword($password);
                $school->setActive(false);
                $school->setCreatedAt($now);
                $school->setUpdatedAt($now);
                $school->setRoles([Permissions::ROLE_SCHOOL_NAME]);
                $statistics = new Statistics();
                $statistics->setOwner($school);
                $this->entityManager->persist($school);
                $this->entityManager->persist($statistics);
                $this->entityManager->flush();

                $flow->reset(); // remove step data from the session

                return $this->redirectToRoute('app_login'); // redirect when done
            }
        }

        return $this->render('register/index.html.twig', [
            'form' => $form->createView(),
            'flow' => $flow,
        ]);
    }
}
