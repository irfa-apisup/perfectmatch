<?php

namespace App\Controller;

use App\Constants\Permissions;
use App\Constants\Settings;
use App\Entity\Setting;
use App\Repository\SettingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SettingsController extends AbstractController
{

    #[Route('/settings', name: 'admin_settings')]
    public function settings(SettingRepository $settingsRepository, Settings $setting): Response
    {
        $settings = $settingsRepository->findAll();
        $jobs = $settingsRepository->findOneBy(['name' => 'jobs']);

        $form = $this->createFormBuilder($jobs)
            ->add('value', ChoiceType::class, [
                'label' => 'label.setting.value',
                'required' => false,
                'attr' => [
                    'class' => 'select2-setting',
                ],
            ])
            ->getForm();

        return $this->render('admin/settings.html.twig', ['appSettings' => $settings, 'form' => $form->createView()]);
    }

    #[Route('/settings/get/jobs', name: 'admin_settings_get_jobs')]
    public function settings_get_jobs(SettingRepository $settingRepository): Response
    {
        $jobs = $settingRepository->findOneBy(['name' => 'jobs'])->getValue();
        return new JsonResponse($jobs);
    }

    #[Route('/settings/set/jobs', name: 'admin_settings_set_jobs', methods: ['POST'])]
    public function settings_set_jobs(SettingRepository $settingRepository, EntityManagerInterface $entityManager, Request $request): Response
    {
        $jobs = $settingRepository->findOneBy(['name' => 'jobs']);
        $jobs->setValue($request->get('data'));
        $entityManager->persist($jobs);
        $entityManager->flush();
        return new JsonResponse('Success');
    }

    #[Route('/settings/{id}/{state}', name: 'admin_settings_actions')]
    public function settings_actions(Setting $setting, string $state, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted(Permissions::ROLE_ADMIN_NAME);
        $setting->setValue('true' == $state ? true : false);
        $entityManager->persist($setting);
        $entityManager->flush();

        return new JsonResponse('Success');
    }
}
