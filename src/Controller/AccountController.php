<?php

namespace App\Controller;

use App\Common\MailerHelper;
use App\Constants\Permissions;
use App\Constants\Settings;
use App\Entity\Compagny\Compagny;
use App\Entity\Compagny\CompagnyInternship;
use App\Entity\Contact;
use App\Entity\Message;
use App\Entity\Report;
use App\Entity\School\Degree;
use App\Entity\School\School;
use App\Entity\Trainee\Trainee;
use App\Entity\User;
use App\Form\Compagny\CompagnyFlow;
use App\Form\School\SchoolFlow;
use App\Form\Trainee\TraineeFlow;
use App\Repository\ReportRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class AccountController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    // Get all informations for user account
    #[Route('/account', name: 'account')]
    public function index(): Response
    {
        $user = $this->getUser();
        $data = [];

        if($user instanceof School){
            $school = $user;
            $trainees = $school->getTrainees();
            $activeTrainee = $trainees->filter(function(Trainee $trainee){ return $trainee->getActive() === true;})->count();
            $inavtiveTrainee = $trainees->filter(function(Trainee $trainee){ return $trainee->getActive() === false;})->count();
            $data['trainee'] = ["active" => $activeTrainee, 'inactive' => $inavtiveTrainee];

            $degreeActive = $school->getDegrees()->filter(function(Degree $degree){ return $degree->getActive() === true;})->count();
            $degreeInactive = $school->getDegrees()->filter(function(Degree $degree){ return $degree->getActive() === false;})->count();
            $data['degree'] = ["active" => $degreeActive, 'inactive' => $degreeInactive];
        }else if($user instanceof Compagny && !$this->isGranted(Permissions::ROLE_ADMIN_NAME)){
            $compagny = $user;
            $internships = $compagny->getInternships();
            $activeInternship = $internships->filter(function(CompagnyInternship $internship) {return $internship->getActive() === true;})->count();
            $inactiveInternship = $internships->filter(function(CompagnyInternship $internship) {return $internship->getActive() === false;})->count();
            $data['internship'] = ["active" => $activeInternship, 'inactive' => $inactiveInternship];
        }

        return $this->render('account/index.html.twig', [
            'data' => $data
        ]);
    }

    #[Route('/account/settings', name: 'account_settings')]
    public function settings(UserPasswordHasherInterface $hash,TraineeFlow $flow, CacheManager $cacheManager, UploaderHelper $helper, EntityManagerInterface $entityManager, string $projectDir, Request $request): Response
    {
        $this->denyAccessUnlessGranted(Permissions::ROLE_TRAINEE_NAME);
        /**
         * @var Trainee
         */
        $user = $this->getUser();
        $flow->setGenericFormOptions(['update' => true]);
        $flow->bind($user);
        $form = $flow->createForm();
        $avatar = '';
        $trainee = $form->getData();
        if (null != $trainee->getAvatarFilename()) {
            $filename = $trainee->getAvatarFilename();

            $path = $projectDir.'/assets/trainee/avatar/'.$filename;
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $avatar = 'data:image/'.$type.';base64,'.base64_encode($data);
        }

        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);
            /**
             * @var Trainee $trainee
             */
            if ($flow->nextStep()) {
                // form for the next step

                $form = $flow->createForm();
            } else {
                if ('' != $user->getPlainPassword()) {
                    $password = $hash->hashPassword($user, $user->getPlainPassword());
                    $user->eraseCredentials();
                    $user->setPassword($password);
                }

                if ($user->getAvatarFile() instanceof UploadedFile) {
                    $cacheManager->remove($helper->asset($user, 'avatarFile'));
                }

                $user->setUpdatedAt(new DateTime('now'));
                $user->setInitialized(true);
                $entityManager->persist($user);
                $entityManager->flush();

                $flow->reset(); // remove step data from the session

                return $this->redirectToRoute('account_settings'); // redirect when done
            }
        }

        return $this->renderForm('account/settings.html.twig', [
            // 'compagny' => $compagny,
            'form' => $form,
            'flow' => $flow,
            'avatar' => $avatar,
        ]);
    }

    // Disable user account by email and redirect to homepage
    #[Route('/account/disable-my-account', name: 'account_disable')]
    public function disable(Request $request, UserRepository $userRepository): Response
    {
        if ('POST' == $request->getMethod()) {
            /**
             * @var User $user
             */
            $user = $this->getUser();
            if ($user instanceof Trainee) {
                /**
                 * @var Trainee $user
                 */
                $user = $this->getUser();
                $user->setInitialized(false);
            }
            $user->setActive(false);
            $this->entityManager->persist($user);
            $this->entityManager->flush($user);

            return $this->redirectToRoute('app_logout');
        }

        return $this->redirectToRoute('home');
    }

    #[Route('/account/messaging/{id?}', name: 'account_messaging', methods: ['GET', 'POST'])]
    public function messaging(?User $target, Request $request, EntityManagerInterface $entityManager, UserRepository $userRepository, MailerHelper $helper, Settings $settings)
    {
        if ($settings->isAnonymisationEnabled()) {
            return $this->redirectToRoute('account');
        }
        /**
         * @var User $user
         */
        $user = $this->getUser();

        if ('POST' == $request->getMethod() && null != $target) {
            $message = $request->request->get('message');
            if ('' != trim($message, ' ')) {
                $msg = new Message();
                $msg->setCreatedAt(new DateTime('now'));
                $msg->setIsRead(false);
                $msg->setMessage($message);
                $msg->setRecipient($target);
                $msg->setSender($user);
                $entityManager->persist($msg);
                $entityManager->flush($msg);
                $contact = new Contact();

                if ($user instanceof Compagny) {
                    $contact->setName($user->getName());
                    $contact->setEmail($user->getContactMail());
                    $contact->setPhone($user->getContactPhone());
                    $contact->setMessage($message);
                    $contact->setRecipient($target->getEmail());
                    $helper->sendMessagingAlert($contact, $user->getLogoFilename());
                }

                if ($target instanceof Trainee && $user instanceof Compagny) {
                    $user->getStatistics()->increaseTraineeContact();
                } elseif ($target instanceof Compagny && $user instanceof Trainee) {
                    $user->getStatistics()->increaseCompagnyContact();
                }

                $this->entityManager->persist($user);
                $this->entityManager->flush();

                return $this->redirectToRoute('account_messaging');
            }
        }

        if ('POST' == $request->getMethod() && null == $target) {
            $targetId = $request->request->get('target');
            $message = $request->request->get('message');

            if ('' != trim($message, ' ')) {
                $msg = new Message();
                $msg->setCreatedAt(new DateTime('now'));
                $msg->setIsRead(false);
                $msg->setMessage($message);
                $msg->setRecipient($userRepository->find($targetId));
                $msg->setSender($user);
                $entityManager->persist($msg);
                $entityManager->flush($msg);
            }
        }

        $messages = $userRepository->findAllMessage($user->getId());
        $messaging = [];
        foreach ($messages as $message) {
            /**
             * @var Message $message
             */
            $sender = $message->getSender();
            $recipient = $message->getRecipient();

            $id = -1;
            $name = '';
            $sent = false;
            if ($recipient->getId() != $user->getId()) {
                $id = $recipient->getId();
                $sent = true;
                if ($recipient instanceof Trainee) {
                    $name = $recipient->getFirstName().' '.$recipient->getLastName();
                } elseif ($recipient instanceof Compagny || $recipient instanceof School) {
                    $name = $recipient->getContactFirstName().' '.$recipient->getContactLastName();
                }
            } else {
                $id = $sender->getId();
                $sent = false;
                if ($sender instanceof Trainee) {
                    $name = $sender->getFirstName().' '.$sender->getLastName();
                } elseif ($sender instanceof Compagny || $sender instanceof School) {
                    $name = $sender->getContactFirstName().' '.$sender->getContactLastName();
                }
            }
            if (!isset($messaging[$id]['messages'])) {
                $messaging[$id] = ['messages' => [], 'is_read' => false];
            }
            array_push($messaging[$id]['messages'], ['message' => $message->getMessage(), 'sent' => $sent, 'date' => $message->getCreatedAt()]);
            $messaging[$id]['is_read'] = $message->getIsRead();
            $messaging[$id]['name'] = $name;
        }

        return $this->renderForm('account/messaging.html.twig', [
            'messaging' => $messaging,
            'creating' => null == $target ? false : true,
            'target' => $target,
        ]);
    }

    #[Route('/account/messaging/report/{id}', name: 'account_messaging_report', methods: ['POST'])]
    public function messagingReport(Trainee $trainee, Request $request, EntityManagerInterface $entityManager, MailerHelper $mailerHelper){

        $this->denyAccessUnlessGranted(Permissions::ROLE_COMPAGNY_NAME);

        if($request->getMethod() == "POST"){
            $message = $request->request->get('message');

            $company = $this->getUser();
            $report = new Report();
            $report->setMessage($message);
            $report->setTrainee($trainee);
            $report->setActive(true);
            $report->setCompany($company);

            try{
                $mailerHelper->sendReportingEmail($company, $trainee, $message);
            }catch (Exception $e){}

            $entityManager->persist($report);
            $entityManager->flush();
        }

        return $this->redirect($request->headers->get('referer'));
    }


    #[Route('/account/contact/{id}/{idi?}', name: 'account_contact_request', defaults: ["idi" => null])]
    #[ParamConverter('internship', CompagnyInternship::class, options: ['id' => 'idi'])]
    public function contactRequest(User $target, CompagnyInternship $internship = null, Settings $settings, MailerHelper $helper, Request $request)
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();
        if ($settings->isAnonymisationEnabled()) {
            $userContact = new Contact();
            $targetContact = new Contact();
            if ($user instanceof Trainee && $target instanceof Compagny) {
                $userContact
                    ->setEmail($user->getEmail())
                    ->setFirstname($user->getFirstname())
                    ->setLastname($user->getLastname())
                    ->setPhone($user->getPhone());
                $targetContact
                    ->setEmail($target->getContactMail())
                    ->setFirstname($target->getContactFirstname())
                    ->setLastname($target->getContactLastname())
                    ->setPhone($target->getContactPhone());
                $helper->sendContactRequestAlert($userContact, $targetContact, $internship);
            } elseif ($user instanceof Compagny && $target instanceof Trainee) {
                $userContact
                    ->setEmail($user->getContactMail())
                    ->setFirstname($user->getContactFirstname())
                    ->setLastname($user->getContactLastname())
                    ->setPhone($user->getContactPhone());
                $targetContact
                    ->setEmail($target->getEmail())
                    ->setFirstname($target->getFirstname())
                    ->setLastname($target->getLastname())
                    ->setPhone($target->getPhone());
                $helper->sendContactRequestAlert($userContact, $targetContact);
            }

        }
        $user->getStatistics()->increaseRequest();
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->redirect($request->headers->get('referer'));
    }
}
