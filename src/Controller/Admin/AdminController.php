<?php

namespace App\Controller\Admin;

use App\Common\MailerHelper;
use App\Constants\Permissions;
use App\Constants\Settings;
use App\Entity\Compagny\Compagny;
use App\Entity\Report;
use App\Entity\School\School;
use App\Entity\Setting;
use App\Entity\Trainee\Trainee;
use App\Repository\Compagny\CompagnyRepository;
use App\Repository\ReportRepository;
use App\Repository\School\DegreeRepository;
use App\Repository\School\SchoolRepository;
use App\Repository\SettingRepository;
use App\Repository\StatisticsRepository;
use App\Repository\Trainee\TraineeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin')]
class AdminController extends AbstractController
{

    private MailerHelper $mailerHelper;

    public function __construct(MailerHelper $mailerHelper)
    {
        $this->mailerHelper = $mailerHelper;
    }

    #[Route('/', name: 'admin')]
    public function index(TraineeRepository $traineeRepository, SchoolRepository $schoolRepository, CompagnyRepository $compagnyRepository, DegreeRepository $degreeRepository, StatisticsRepository $statisticsRepository): Response
    {
        $schools = ["active" => $schoolRepository->countActive(), "inactive" => $schoolRepository->countInactive()];
        $compagnies = ["active" => $compagnyRepository->countActive(), "inactive" => $compagnyRepository->countInactive()];
        $trainees = ["active" => $traineeRepository->countActive(), "inactive" => $traineeRepository->countInactive()];
        $degrees = ["active" => $degreeRepository->countActive(), "inactive" => $degreeRepository->countInactive()];
        $r = $statisticsRepository->getAll();
        $sums = array();
        foreach ($r as $s){
            foreach (array_keys($sums + $s) as $stats) {
                $sums[$stats] = (isset($sums[$stats]) ? $sums[$stats] : 0) + (isset($s) ? $s[$stats] : 0);
            }
        }

        return $this->render('admin/index.html.twig', [
            'schools' => $schools,
            'compagnies' => $compagnies,
            'trainees' => $trainees,
            'degrees' => $degrees,
            'statistics' => $sums
        ]);
    }

    #[Route('/validation', name: 'admin_validation')]
    public function school(SchoolRepository $schoolRepository, CompagnyRepository $compagnyRepository, TraineeRepository $traineeRepository): Response
    {
        $schools = $schoolRepository->findSchoolOrderActive();
        $compagnies = $compagnyRepository->findCompagnyOrderActive();
        $trainees = $traineeRepository->findTraineeOrderActive();

        return $this->render('admin/validation.html.twig', [
            'schools' => $schools,
            'compagnies' => $compagnies,
            'trainees' => $trainees
        ]);
    }

    #[Route('/reporting', name: 'admin_reporting')]
    public function reporting(ReportRepository $reportRepository): Response
    {
        $reports = $reportRepository->findBy(['active' => true]);


        return $this->render('admin/report.html.twig', [
            'reports' => $reports
        ]);
    }

    #[Route('/reporting/disable/{id}', name: 'admin_disable_report')]
    public function disableReport(Report $report, EntityManagerInterface $entityManager, Request $request): Response
    {
        $report->setActive(false);
        $entityManager->persist($report);
        $entityManager->flush();
        return $this->redirect($request->headers->get('referer'));
    }


    #[Route('/school/{id}/validation/{state}', name: 'admin_school_validation')]
    public function school_validate(Request $request, School $school, EntityManagerInterface $entityManager, string $state): Response
    {
        $school->setActive(json_decode($state));
        $entityManager->persist($school);
        $entityManager->flush();
        if(true == json_decode($state)){
            $this->mailerHelper->sendValidationConfirmation($school->getContactMail());
        }

        return $this->redirect($request->headers->get('referer'));
    }

    #[Route('/trainee/{id}/validation/{state}', name: 'admin_trainee_validation')]
    public function trainee_validate(Request $request, Trainee $trainee, EntityManagerInterface $entityManager, string $state): Response
    {
        $trainee->setActive(json_decode($state));
        $entityManager->persist($trainee);
        $entityManager->flush();
        if(true == json_decode($state)) {
            $this->mailerHelper->sendValidationConfirmation($trainee->getEmail());
        }

        return $this->redirect($request->headers->get('referer'));
    }

    #[Route('/compagny/{id}/validation/{state}', name: 'admin_compagny_validation')]
    public function compagnyl_validate(Request $request, Compagny $compagny, EntityManagerInterface $entityManager, string $state): Response
    {
        $compagny->setActive(json_decode($state));
        $entityManager->persist($compagny);
        $entityManager->flush();
        if(true == json_decode($state)) {
            $this->mailerHelper->sendValidationConfirmation($compagny->getContactMail());
        }
        return $this->redirect($request->headers->get('referer'));
    }
}
