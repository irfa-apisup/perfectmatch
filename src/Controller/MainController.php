<?php

namespace App\Controller;

use App\Common\MailerHelper;
use App\Constants\Permissions;
use App\Constants\Settings;
use App\Entity\Compagny\Compagny;
use App\Entity\Contact;
use App\Entity\School\School;
use App\Entity\Statistics;
use App\Entity\Survey;
use App\Entity\User;
use App\Form\Compagny\InternshipSearchType;
use App\Form\ContactType;
use App\Form\School\SchoolFlow;
use App\Form\SearchType;
use App\Form\SurveyType;
use App\Repository\Compagny\CompagnyImageRepository;
use App\Repository\Compagny\CompagnyInternshipRepository;
use App\Repository\Compagny\CompagnyRepository;
use App\Repository\School\SchoolImageRepository;
use App\Repository\School\SchoolRepository;
use App\Repository\UserRepository;
use App\Security\RoleActivator;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use function PHPUnit\Framework\throwException;

class MainController extends AbstractController
{
    private CompagnyRepository $compagnyRepository;
    private CompagnyInternshipRepository $compagnyInternshipRepository;
    private Settings $settings;

    public function __construct(CompagnyRepository $compagnyRepository, CompagnyInternshipRepository $compagnyInternshipRepository, Settings $settings)
    {
        $this->compagnyRepository = $compagnyRepository;
        $this->compagnyInternshipRepository = $compagnyInternshipRepository;
        $this->settings = $settings;
    }

    #[Route('/', name: 'app_home')]
    public function index(UserRepository $userRepository, SchoolRepository $schoolRepository): Response
    {
        $internships = $this->compagnyInternshipRepository->findBy(['active' => true], null, 10);
        $compagnies = $this->compagnyRepository->findCompagnyOrderActive(10);
        $data = $schoolRepository->findOneByRoleThatSucksLess('admin')[0];

        return $this->render('main/index.html.twig', [
            'compagnies' => $compagnies,
            'internships' => $internships,
            'data' => $data,
        ]);
    }

    #[Route('/referer', name: 'app_referer')]
    public function referer(UserRepository $userRepository, SchoolFlow $flow, UserPasswordHasherInterface $hash, EntityManagerInterface $entityManager): Response
    {
        $users = $userRepository->findBy([], null, 1);
        if (null != $users) {
            return $this->redirectToRoute('app_home');
        }

        $school = new School();
        $flow->bind($school);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();
            } else {
                $password = $hash->hashPassword($school, $school->getPassword());
                $school->setPassword($password);
                $school->setActive(true);
                $school->setUpdatedAt(new DateTime('now'));
                $school->setCreatedAt(new DateTime('now'));
                $school->setRoles([Permissions::ROLE_ADMIN_NAME]);
                $statistics = new Statistics();
                $statistics->setOwner($school);
                $entityManager->persist($school);
                $entityManager->persist($statistics);

                // Initialized default settings
                $entityManager->persist($this->settings->getAnonimisation());
                $entityManager->flush();

                $flow->reset(); // remove step data from the session

                return $this->redirectToRoute('app_login'); // redirect when done
            }
        }

        return $this->render('main/referer.html.twig', [
            'form' => $form->createView(),
            'flow' => $flow,
        ]);
    }

    #[Route('/degrees/{id}', name: 'degrees', methods: ['GET'])]
    public function listDegrees(Request $request, School $school)
    {
        $degrees = $school->getDegrees();
        $result = [];
        foreach ($degrees as $degree) {
            if ($degree->getActive()) {
                $result[] = [
                    'id' => $degree->getId(),
                    'name' => $degree->getName(),
                ];
            }
        }

        return new JsonResponse($result);
    }

    #[Route('/internship', name: 'app_home_internship')]
    public function internship(Request $request, CompagnyInternshipRepository $compagnyInternshipRepository): Response
    {
        $internships = [];

        $form = $this->createForm(InternshipSearchType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
//            $trainees = $traineeRepository->searchByFilter($data["sector"], $data["zone"], $data["startPeriod"], $data["endPeriod"]);
            $internships = $compagnyInternshipRepository->searchByFilter($data['title'], $data['startPeriod'], $data['endPeriod']);
        } else {
            $internships = $compagnyInternshipRepository->findBy(['active' => true]);
        }

        return $this->render('main/internshipSearch.html.twig', [
            'internships' => $internships,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/contact', name: 'app_home_contact')]
    public function contact(Request $request, MailerHelper $helper): Response
    {
        $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $helper->sendContactMail($contact);

            return $this->redirectToRoute('app_home_contact', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('main/contact.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/toolbox', name: 'app_home_toolbox')]
    public function toolbox(): Response
    {
        return $this->render('main/toolbox.html.twig', []);
    }

    #[Route('/term', name: 'app_home_term')]
    public function term(): Response
    {
        return $this->render('main/term.html.twig', []);
    }

    #[Route('/notice', name: 'app_home_notice')]
    public function notice(): Response
    {
        return $this->render('main/notice.html.twig', []);
    }

    #[Route('/survey', name: 'app_home_survey')]
    public function survey(Request $request, EntityManagerInterface $entityManager): Response
    {
        if (null == $this->getUser()) {
            return $this->redirectToRoute('app_home');
        }

        /**
         * @var User $user
         */
        $user = $this->getUser();
        if ($user->getSurveys()->count() >= 1) {
            return $this->redirectToRoute('app_home');
        }

        $form = $this->createForm(SurveyType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var Survey $survey
             */
            $survey = $form->getData();
            $survey->setOwner($this->getUser());
            $entityManager->persist($survey);
            $entityManager->flush($survey);
            $this->addFlash('success', 'Thank you for responding to our survey');

            return $this->redirectToRoute('app_home');
        }

        return $this->render('main/survey.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/search', name: 'app_search')]
    public function search(Request $request, CompagnyRepository $compagnyRepository, SchoolRepository $schoolRepository): Response
    {
        $compagnies = [];
        $schools = [];

        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if($data["type"] == null){
                $compagnies = $compagnyRepository->searchByFilter($data["name"]);
                $schools = $schoolRepository->searchByFilter($data["name"]);
            }elseif ($data["type"] == School::class){
                $schools = $schoolRepository->searchByFilter($data["name"]);
            }else{
                $compagnies = $compagnyRepository->searchByFilter($data["name"]);
            }

        } else {
            $schools = $schoolRepository->findBy(['active' => true]);
            $compagnies = $compagnyRepository->findBy(['active' => true]);
        }

        return $this->render('main/search.html.twig', [
            'compagnies' => $compagnies,
            'schools' => $schools,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/search/compagny/{id}', name: 'app_search_compagny')]
    public function searchCompagny(Compagny $compagny, CompagnyImageRepository $compagnyImageRepository): Response
    {
        $images = $compagnyImageRepository->findBy(["compagny" => $compagny->getId()]);
        return $this->render('main/searchView.html.twig', [
            'data' => $compagny,
            'images' => $images,
        ]);
    }

    #[Route('/search/school/{id}', name: 'app_search_school')]
    public function searchSchool(School $school, SchoolImageRepository $schoolImageRepository): Response
    {
        $images = $schoolImageRepository->findBy(["school" => $school->getId()]);
        return $this->render('main/searchView.html.twig', [
            'data' => $school,
            'images' => $images,
        ]);
    }

    #[Route('/change-locale/{locale}', name: 'app_home_locale')]
    public function changeLocale($locale, Request $request)
    {
        $request->getSession()->set('_locale', $locale);
        return $this->redirect($request->headers->get('referer'));
    }
}
