<?php

namespace App\Controller;

use App\Repository\School\SchoolRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UtilsController extends AbstractController
{

    public function footer(SchoolRepository $schoolRepository)
    {
        $data = $schoolRepository->findOneByRoleThatSucksLess('admin')[0];
        return $this->render('utils/_footer.html.twig', [
            'data' => $data
        ]);
    }
}
