<?php

namespace App\Controller\School;

use App\Constants\Permissions;
use App\Entity\School\Degree;
use App\Entity\School\School;
use App\Form\School\DegreeType;
use App\Repository\School\DegreeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/account/school/degree')]
class DegreeController extends AbstractController
{
    #[Route('/', name: 'app_degree_index', methods: ['GET'])]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted(Permissions::ROLE_SCHOOL_NAME);
        /**
         * @var School $school
         */
        $school = $this->getUser();

        return $this->render('degree/index.html.twig', [
            'degrees' => $school->getDegrees(),
        ]);
    }

    #[Route('/new', name: 'app_degree_new', methods: ['GET', 'POST'])]
    public function new(Request $request, DegreeRepository $degreeRepository): Response
    {
        $this->denyAccessUnlessGranted(Permissions::ROLE_SCHOOL_NAME);
        /**
         * @var School $school
         */
        $school = $this->getUser();

        $degree = new Degree();
        $form = $this->createForm(DegreeType::class, $degree);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $degree->setSchool($school);
            $degreeRepository->add($degree);

            return $this->redirectToRoute('app_degree_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('degree/new.html.twig', [
            'degree' => $degree,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_degree_show', methods: ['GET'])]
    public function show(Degree $degree): Response
    {
        $this->denyAccessUnlessGranted(Permissions::ROLE_SCHOOL_NAME);
        /**
         * @var School $school
         */
        $school = $this->getUser();

        if (!$school->getDegrees()->contains($degree) && !$this->denyAccessUnlessGranted(Permissions::ROLE_ADMIN_NAME)) {
            throw new AccessDeniedException('Access denied.');
        }

        return $this->render('degree/show.html.twig', [
            'degree' => $degree,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_degree_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Degree $degree, DegreeRepository $degreeRepository): Response
    {
        $this->denyAccessUnlessGranted(Permissions::ROLE_SCHOOL_NAME);
        /**
         * @var School $school
         */
        $school = $this->getUser();

        if (!$school->getDegrees()->contains($degree) && !$this->denyAccessUnlessGranted(Permissions::ROLE_ADMIN_NAME)) {
            throw new AccessDeniedException('Access denied.');
        }

        $form = $this->createForm(DegreeType::class, $degree);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $degreeRepository->add($degree);

            return $this->redirectToRoute('app_degree_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('degree/edit.html.twig', [
            'degree' => $degree,
            'form' => $form,
        ]);
    }
}
