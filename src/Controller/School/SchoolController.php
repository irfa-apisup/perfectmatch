<?php

namespace App\Controller\School;

use App\Common\MailerHelper;
use App\Constants\Permissions;
use App\Entity\School\School;
use App\Entity\Trainee\Trainee;
use App\Entity\User;
use App\Form\School\SchoolFlow;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

#[Route('/account/school')]
class SchoolController extends AbstractController
{
    /**
     * Edit a school.
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     * @param School                 $school
     *
     * @return Response
     */
    #[Route('/edit', name: 'school_edit', methods: ['GET', 'POST'])]
    public function edit(UserPasswordHasherInterface $hash, UploaderHelper $helper, CacheManager $cacheManager, EntityManagerInterface $entityManager, SchoolFlow $flow): Response
    {
        // Deny acces if user d'ont have permission
        $this->denyAccessUnlessGranted(Permissions::ROLE_SCHOOL_NAME);

        /**
         * @var School $school
         */
        $school = $this->getUser();
        $flow->setGenericFormOptions(['update' => true]);
        $flow->bind($school);
        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();
            } else {
                if ('' != $school->getPlainPassword()) {
                    $password = $hash->hashPassword($school, $school->getPlainPassword());
                    $school->eraseCredentials();
                    $school->setPassword($password);
                }

                if ($school->getLogoFile() instanceof UploadedFile) {
                    $cacheManager->remove($helper->asset($school, 'logoFile'));
                }

                foreach ($school->getImages() as $image) {
                    if (null == $image->getFilename()) {
                        $entityManager->remove($image);
                        $entityManager->flush();
                    }
                }

                $school->setUpdatedAt(new DateTime('now'));
                $entityManager->persist($school);
                $entityManager->flush();

                $flow->reset(); // remove step data from the session

                return $this->redirectToRoute('school_edit'); // redirect when done
            }
        }

        return $this->renderForm('school/edit.html.twig', [
            // 'school' => $school,
            'form' => $form,
            'flow' => $flow,
        ]);
    }

    #[Route('/validate', name: 'school_validate', methods: ['GET', 'POST'])]
    public function validate(): Response
    {
        $this->denyAccessUnlessGranted(Permissions::ROLE_SCHOOL_NAME);
        /**
         * @var School $school
         */
        $school = $this->getUser();
        return $this->render('school/validation.html.twig', [
            'trainees' => $school->getTrainees()->toArray(),
        ]);
    }

    #[Route('/validation/{id}/{state}', name: 'school_validation_trainee', methods: ['GET', 'POST'])]
    public function validationTrainee(Trainee $trainee, Request $request, EntityManagerInterface $entityManager, string $state, MailerHelper $mailerHelper) {

        $this->denyAccessUnlessGranted(Permissions::ROLE_SCHOOL_NAME);

        /**
         * @var School $school
         */
        $school = $this->getUser();

        if($school->getTrainees()->contains($trainee)){
            $trainee->setActive(json_decode($state));
            $entityManager->persist($trainee);
            $entityManager->flush();
            if(true == json_decode($state)) {
                $mailerHelper->sendValidationConfirmation($trainee->getEmail());
            }
        }else{
            return new UnauthorizedHttpException();
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Delete a school.
     *
     * @param Request                $request
     * @param School                 $school
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    #[Route('/{id}', name: 'school_delete', methods: ['DELETE'])]
    public function delete(Request $request, School $school, EntityManagerInterface $entityManager): Response
    {
        // Deny acces if user d'ont have permission
        $this->denyAccessUnlessGranted(Permissions::ROLE_SCHOOL_NAME);

        /**
         * @var User $user
         */
        $user = $this->getUser();
        // $school = $user->getSchool();

        // If user is owner delete school
        if ($this->isCsrfTokenValid('delete'.$school->getId(), $request->request->get('_token'))) {
            //    $school->setActive(false);
            $entityManager->persist($school);
            $entityManager->flush();
        }

        return $this->redirectToRoute('school_index', [], Response::HTTP_SEE_OTHER);
    }
}
