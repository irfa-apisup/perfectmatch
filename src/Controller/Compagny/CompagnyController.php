<?php

namespace App\Controller\Compagny;

use App\Constants\Permissions;
use App\Constants\Settings;
use App\Entity\Compagny\Compagny;
use App\Entity\Trainee\Trainee;
use App\Form\Compagny\CompagnyFlow;
use App\Form\Trainee\TraineeSearchType;
use App\Repository\Trainee\TraineeRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

#[Route('/account/compagny')]
class CompagnyController extends AbstractController
{
    /**
     * Edit a compagny.
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     * @param Compagny               $compagny
     *
     * @return Response
     */
    #[Route('/edit', name: 'compagny_edit', methods: ['GET', 'POST'])]
    public function edit(CompagnyFlow $flow, UserPasswordHasherInterface $hash, CacheManager $cacheManager, UploaderHelper $helper, EntityManagerInterface $entityManager): Response
    {
        // Deny acces if user d'ont have permission
        $this->denyAccessUnlessGranted(Permissions::ROLE_COMPAGNY_NAME);

        /**
         * @var Compagny $compagny
         */
        $compagny = $this->getUser();
        $flow->setGenericFormOptions(['update' => true]);
        $flow->bind($compagny);
        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();
            } else {
                if ('' != $compagny->getPlainPassword()) {
                    $password = $hash->hashPassword($compagny, $compagny->getPlainPassword());
                    $compagny->eraseCredentials();
                    $compagny->setPassword($password);
                }

                if ($compagny->getLogoFile() instanceof UploadedFile) {
                    $cacheManager->remove($helper->asset($compagny, 'logoFile'));
                }

                foreach ($compagny->getImages() as $image) {
                    if (null == $image->getFilename()) {
                        $entityManager->remove($image);
                        $entityManager->flush();
                    }
                }

                $compagny->setUpdatedAt(new DateTime('now'));
                $entityManager->persist($compagny);
                $entityManager->flush();

                $flow->reset(); // remove step data from the session

                return $this->redirectToRoute('compagny_edit'); // redirect when done
            }
        }

        return $this->renderForm('compagny/edit.html.twig', [
            // 'compagny' => $compagny,
            'form' => $form,
            'flow' => $flow,
        ]);
    }

    /**
     * Search trainee profil.
     */
    #[Route('/trainee', name: 'compagny_trainee', methods: ['GET', 'POST'])]
    public function trainee(Request $request, TraineeRepository $traineeRepository)
    {
        if (!$this->grantAccess()) {
            return $this->render('compagny/traineeSearch.html.twig', [
                'access' => false,
            ]);
        }

        $trainees = [];

        $form = $this->createForm(TraineeSearchType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $trainees = $traineeRepository->searchByFilter($data['job'], $data['zone'], $data['startPeriod'], $data['endPeriod']);
        } else {
            $trainees = $traineeRepository->findBy(['initialized' => true]);
        }

        return $this->renderForm('compagny/traineeSearch.html.twig', [
            'access' => true,
            'trainees' => $trainees,
            'form' => $form,
        ]);
    }

    /**
     * Search trainee profil.
     */
    #[Route('/trainee/{id}', name: 'compagny_trainee_show', methods: ['GET', 'POST'])]
    public function showTrainee(Trainee $trainee, Settings $settings, EntityManagerInterface $entityManager)
    {
        if (!$this->grantAccess()) {
            return $this->render('compagny/traineeSearch.html.twig', [
                'access' => false,
            ]);
        }

        /**
         * @var Compagny $compagny
         */
        $compagny = $this->getUser();
        $compagny->getStatistics()->increaseTraineeProfil();
        $entityManager->persist($compagny);
        $entityManager->flush();

        return $this->render('compagny/traineeView.html.twig', [
            'trainee' => $trainee,
            'anonymisation' => $settings->isAnonymisationEnabled(),
        ]);
    }

    /**
     * Search trainee file.
     */
    #[Route('/trainee/document/{filename}', name: 'compagny_trainee_download_document', methods: ['GET'])]
    public function downloadTraineeCv(Request $request, string $projectDir)
    {
        $this->grantAccess();
        return new BinaryFileResponse($projectDir.'/assets/trainee/documents/'.$request->get('filename'));
    }

    /**
     * Search trainee file.
     */
    #[Route('/internship/document/{filename}', name: 'compagny_internhsip_download_document', methods: ['GET'])]
    public function downloadInternshipDocument(Request $request, string $projectDir)
    {
        return new BinaryFileResponse($projectDir.'/assets/internship/documents/'.$request->get('filename'));
    }

    private function grantAccess()
    {
        // Deny acces if user d'ont have permission
        $this->denyAccessUnlessGranted(Permissions::ROLE_COMPAGNY_NAME);
        /**
         * @var Compagny $compagny
         */
        $compagny = $this->getUser();
        $internships = $compagny->getInternships();
        if ($internships->count() <= 0) {
            return false;
        } elseif ($internships->count() >= 0) {
            $access = false;
            foreach ($internships as $internship) {
                if ($internship->getActive()) {
                    $access = true;
                    break;
                }
            }
            if (!$access) {
                return false;
            }
        }

        return true;
    }
}
