<?php

namespace App\Controller\Compagny;

use App\Constants\Permissions;
use App\Entity\Compagny\Compagny;
use App\Entity\Compagny\CompagnyInternship;
use App\Form\Compagny\CompagnyInternshipType;
use Doctrine\ORM\EntityManagerInterface;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\UploadHandler;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use function PHPUnit\Framework\isNull;

#[Route('/account/compagny')]
class CompagnyInternshipController extends AbstractController
{
    /**
     * Get all compagny internships.
     *
     * @param Compagny $compagny
     *
     * @return Response
     */
    #[Route('/internship', name: 'compagny_internship_index', methods: ['GET'])]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted(Permissions::ROLE_COMPAGNY_NAME);
        /**
         * @var Compagny $compagny
         */
        $compagny = $this->getUser();

        return $this->render('compagny_internship/index.html.twig', [
            'internships' => $compagny->getInternships(),
        ]);
    }

    /**
     * Create a new internship.
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     * @param Compagny               $compagny
     *
     * @return Response
     */
    #[Route('/internship/new', name: 'compagny_internship_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted(Permissions::ROLE_COMPAGNY_NAME);

        /**
         * @var Compagny $compagny
         */
        $compagny = $this->getUser();

        $compagnyInternship = new CompagnyInternship();
        $form = $this->createForm(CompagnyInternshipType::class, $compagnyInternship);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $compagnyInternship->setCompagny($compagny);
            $compagnyInternship->setUpdatedAt(new \DateTime('now'));
            $entityManager->persist($compagnyInternship);
            $entityManager->flush();

            return $this->redirectToRoute('compagny_internship_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm(
            'compagny_internship/new.html.twig',
            [
                'internship' => $compagnyInternship,
                'form' => $form,
            ]
        );
    }

    #[Route('/internship/{id}', name: 'compagny_internship_show', methods: ['GET'])]
    public function show(CompagnyInternship $compagnyInternship): Response
    {
        $compagny = $compagnyInternship->getCompagny();

        $this->denyAccessUnlessGranted(Permissions::ROLE_COMPAGNY_NAME);

        return $this->render('compagny_internship/show.html.twig', [
            'compagny' => $compagny,
            'internship' => $compagnyInternship,
        ]);
    }

    /**
     * Edit compagny internship.
     *
     * @param Request                $request
     * @param CompagnyInternship     $compagnyInternship
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    #[Route('/internsip/{id}/edit', name: 'compagny_internship_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CompagnyInternship $compagnyInternship, EntityManagerInterface $entityManager, UploadHandler $handler): Response
    {
        $compagny = $compagnyInternship->getCompagny();

        $this->denyAccessUnlessGranted(Permissions::ROLE_COMPAGNY_NAME);

        $form = $this->createForm(CompagnyInternshipType::class, $compagnyInternship);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($form["deleteFile"]->getData() !== null){
                $handler->remove($compagnyInternship, 'file');
                $compagnyInternship->setFile(null);
                $compagnyInternship->setFilename(null);
            }
            $entityManager->persist($compagnyInternship);
            $entityManager->flush($compagnyInternship);

            return $this->redirectToRoute('compagny_internship_index', ['id' => $compagny->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('compagny_internship/edit.html.twig', [
            'compagny' => $compagny,
            'internship' => $compagnyInternship,
            'form' => $form,
        ]);
    }
}
