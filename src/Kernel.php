<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

     public function getCacheDir()
     {
         if ($this->getEnvironment() === 'test' || $this->getEnvironment() === 'dev') {
             return '/tmp/symfony/' . $this->environment . '/cache/';
         } else {
             return $this->getProjectDir() . '/var/cache/symfony/' . $this->environment . '/cache/';
         }
     }

     public function getLogDir()
     {
         if ($this->getEnvironment() === 'test' || $this->getEnvironment() === 'dev') {
             return '/tmp/symfony/' . $this->environment . '/log/';
         } else {
             return $this->getProjectDir() . '/var/log/symfony/' . $this->environment . '/log/';
         }
     }
}
