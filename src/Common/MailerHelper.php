<?php

namespace App\Common;

use App\Entity\Compagny\CompagnyInternship;
use App\Entity\Contact;
use App\Entity\School\School;
use App\Repository\School\SchoolRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

class MailerHelper
{
    private MailerInterface $mailer;
    private string $emitter;
    private string $projectDir;
    private SessionInterface $session;

    private ?School $referent;

    public function __construct(MailerInterface $mailerInterface, string $projectDir, string $emitter, SchoolRepository $schoolRepository, SessionInterface $session)
    {
        $this->mailer = $mailerInterface;
        $this->emitter = $emitter;
        $this->projectDir = $projectDir;
        $this->session = $session;
        $schools = $schoolRepository->findOneByRoleThatSucksLess('admin');
        $this->referent = count($schools) > 0 ? $schools[0] : null;
    }

    public function sendContactMail(Contact $contact)
    {
        $email = (new TemplatedEmail())
            ->addFrom($this->emitter)
            ->addTo($this->referent->getContactMail())
            ->subject('[PerfectMatch] You have received a contact email')
            ->htmlTemplate('emails/contact.html.twig')
            ->context([
                'firstname' => $contact->getFirstname(),
                'lastname' => $contact->getLastname(),
                'phone' => $contact->getPhone(),
                'message' => $contact->getMessage(),
                'target' => $contact->getEmail(),
            ]);
        try {
            $this->mailer->send($email);
            $this->session->getFlashBag()->add('success', 'Your message has been send');
        } catch (TransportExceptionInterface $e) {
            $this->session->getFlashBag()->add('error', 'We are sorry that a problem has arisen and we will sort it out as soon as possible');
        }
    }

    public function sendMessagingAlert(Contact $contact, ?string $filename)
    {
        $email = (new TemplatedEmail())
            ->addFrom($this->emitter)
            ->addTo($contact->getRecipient())
            ->subject('[PerfectMatch] You have received a contact request')
            ->embed(fopen($this->projectDir.'/public/images/logo/'.$filename ?? '', 'r'), 'logo')
            ->htmlTemplate('emails/messaging.html.twig')
            ->context([
                'name' => $contact->getName(),
                'phone' => $contact->getPhone(),
                'message' => $contact->getMessage(),
                'target' => $contact->getEmail(),
            ]);

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
        }
    }

    public function sendContactRequestAlert(Contact $user, Contact $target, CompagnyInternship $internship = null)
    {
        $email = (new TemplatedEmail())
            ->addFrom($this->emitter)
            ->addTo($this->referent->getContactMail())
            ->subject('[PerfectMatch] You have received a contact request')
            ->htmlTemplate('emails/request.html.twig')
            ->context([
                'fromFirstname' => $user->getFirstname(),
                'fromLastname' => $user->getLastname(),
                'fromPhone' => $user->getPhone(),
                'fromEmail' => $user->getEmail(),
                'toFirstname' => $target->getFirstname(),
                'toLastname' => $target->getLastname(),
                'toPhone' => $target->getPhone(),
                'toEmail' => $target->getEmail(),
                'internship' => $internship,
            ]);
        try {
            $this->mailer->send($email);
            $this->session->getFlashBag()->add('success', 'Your request has been taken into account');
        } catch (TransportExceptionInterface $e) {
        }
    }

    public function sendRegistrationNotification($schoolRegistration, $companyRegistration)
    {
        $email = (new TemplatedEmail())
            ->addFrom($this->emitter)
            ->addTo($this->referent->getContactMail())
            ->subject('[PerfectMatch] You have received a registration summary')
            ->htmlTemplate('emails/registration.html.twig')
            ->context([
                'school' => $schoolRegistration,
                'company' => $companyRegistration,
            ]);
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {}
    }

    public function sendValidationConfirmation($userEmail)
    {
        $email = (new TemplatedEmail())
            ->addFrom($this->emitter)
            ->addTo($userEmail)
            ->subject('[PerfectMatch] Your account has been validated')
            ->htmlTemplate('emails/validation.html.twig');
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {}
    }

    public function sendReportingEmail($company, $trainee, $message)
    {
        $email = (new TemplatedEmail())
            ->addFrom($this->emitter)
            ->addTo($this->referent->getContactMail())
            ->subject('[PerfectMatch] A trainee has been reported ')
            ->htmlTemplate('emails/reporting.html.twig')
            ->embed(fopen($this->projectDir.'/public/images/pf.png', 'r'), 'logo')
            ->context([
                'company' => $company,
                'trainee' => $trainee,
                'message' => $message,
            ]);
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {}
    }
}
