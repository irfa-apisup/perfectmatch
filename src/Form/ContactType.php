<?php

namespace App\Form;

use App\Entity\Contact;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'label.firstname',
                'attr' => [
                    'placeholder' => 'label.firstname',
                ],
            ])
            ->add('lastname', TextType::class, [
                'label' => 'label.lastname',
                'attr' => [
                    'placeholder' => 'label.lastname',
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => 'label.email',
                'attr' => [
                    'placeholder' => 'label.email',
                ],
            ])
            ->add('phone', TextType::class, [
                'label' => 'label.phone',
                'attr' => [
                    'placeholder' => 'label.phone',
                ],
            ])
            ->add('message', TextareaType::class, [
                'label' => 'label.message',
                'attr' => [
                    'placeholder' => 'label.message'
                ],
            ])
            ->add('captcha', CaptchaType::class, [
                'width' => 200,
                'height' => 50,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'label.submit',
                'attr' => [
                    'class' => 'invert-button',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
