<?php

namespace App\Form\School;

use App\Form\RegisterType;
use Craue\FormFlowBundle\Form\FormFlow;
use Symfony\Contracts\Translation\TranslatorInterface;

class SchoolFlow extends FormFlow
{
    protected $allowDynamicStepNavigation = true;

    public function __construct(TranslatorInterface $translator)
    {
        $translator->trans('label.user');
        $translator->trans('label.school');
        $translator->trans('label.other');
    }

    protected function loadStepsConfig()
    {
        return [
            [
                'label' => 'label.user',
                'form_type' => RegisterType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ],
            [
                'label' => 'label.school',
                'form_type' => SchoolType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ],
            [
                'label' => 'label.other',
                'form_type' => SchoolType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function isStepDone($stepNumber)
    {
        if (null != $this->getFormData() && $this->getFormData()->getId()) {
            return true;
        }

        return parent::isStepDone($stepNumber);
    }
}
