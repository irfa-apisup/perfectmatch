<?php

namespace App\Form\School;

use App\Entity\School\Degree;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DegreeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'label.name',
                'help' => 'help.required',
                'attr' => [
                    'placeholder' => 'label.name',
                ],
            ])
            ->add('description', TextareaType::class, [
                'label' => 'label.form.degree.description',
                'help' => 'help.required',
                'attr' => [
                    'placeholder' => 'placeholder.form.degree.description',
                ],
            ])
            ->add('job', TextType::class, [
                'label' => 'label.degree.job',
                'help' => 'help.required',
                'attr' => [
                    'placeholder' => 'label.degree.job',
                ],
            ])
            ->add('active', CheckboxType::class, [
                'required' => false,
                'label' => 'label.active',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Degree::class,
        ]);
    }
}
