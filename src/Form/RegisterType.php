<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegisterType extends AbstractType
{
    private $router;

    public function __construct(UrlGeneratorInterface $urlGeneratorInterface)
    {
        $this->router = $urlGeneratorInterface;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $requireOnUpdate = !$options['update'];

        $builder
            ->add('email', EmailType::class, [
                'label' => 'label.email',
                'help' => 'help.required',
                'attr' => [
                    'placeholder' => 'label.email',
                ],
            ])
            ->add($requireOnUpdate ? 'password' : 'plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'errors.password',
                'required' => $requireOnUpdate,
                'first_options' => [
                    'label' => 'label.password',
                    'help' => 'help.required',
                    'attr' => [
                        'placeholder' => 'label.password',
                        'class' => 'inputPassword1'
                    ],
                ],
                'second_options' => [
                    'label' => 'label.confirmPassword',
                    'help' => 'help.required',
                    'attr' => [
                        'placeholder' => 'label.confirmPassword',
                        'class' => 'inputPassword2'
                    ],
                ],
            ]);

        if($requireOnUpdate){
            $builder->add('rgpd', CheckboxType::class, [
                'required' => true,
                'label' => "label.term",
                'label_html' => true,
                'mapped' => false,
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'update' => false,
        ]);
    }
}
