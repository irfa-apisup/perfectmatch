<?php

namespace App\Form\Trainee;

use App\Entity\Document;
use App\Entity\School\Degree;
use App\Entity\School\School;
use App\Entity\Trainee\Trainee;
use App\Entity\User;
use App\Form\DocumentType;
use App\Form\LanguageType;
use App\Form\PeriodType;
use Doctrine\ORM\EntityRepository;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use libphonenumber\PhoneNumberUtil;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;

class TraineeType extends AbstractType
{

    private PhoneNumberUtil $numberUtil;
    private Request $request;

    public function __construct(PhoneNumberUtil $phoneNumberUtil, RequestStack $requestStack)
    {
        $this->numberUtil = $phoneNumberUtil;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        switch ($options['flow_step']) {
            case 2:
                $builder
                    ->add('firstname', TextType::class, [
                        'label' => 'label.firstname',
                        'help' => 'help.required',
                        'attr' => [
                            'placeholder' => 'label.firstname',
                        ],
                    ])
                    ->add('lastname', TextType::class, [
                        'label' => 'label.lastname',
                        'help' => 'help.required',
                        'attr' => [
                            'placeholder' => 'label.lastname',
                        ],
                    ])
                    ->add('phone', PhoneNumberType::class, array('widget' => PhoneNumberType::WIDGET_COUNTRY_CHOICE, 'country_choices' => array('AT', 'BE', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'HU', 'IE', 'IT', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE') , 'preferred_country_choices' => array('FR', 'DE', 'IT', 'LT', 'PT', 'ES'), 'help' => 'help.required'))
                    ->add('Period', CollectionType::class, [
                        'label' => 'label.form.trainee.period',
                        'required' => true,
                        'entry_type' => PeriodType::class,
                        'allow_add' => true,
                        'allow_delete' => true,
                        'by_reference' => false,
                    ])
                    ->add('zone', ChoiceType::class, [
                        'label' => 'label.trainee.geographicalarea',
                        'help' => 'help.required',
                        'multiple' => true,
                        'choices' => [
                            'label.area.regional' => 'Regional',
                            'label.area.national' => 'National',
                            'label.area.international' => 'International',
                        ],
                        'attr' => [
                            'class' => 'select2-multiple',
                        ],
                    ])
                    ->add('school', EntityType::class, [
                        'label' => 'label.form.trainee.selectschool',
                        'help' => 'help.required',
                        'class' => School::class,
                        'query_builder' => function (EntityRepository $er) {
                            return $er->createQueryBuilder('s')
                                ->innerJoin('s.Degrees', 'd')
                                ->where('d.school = s.id')
                                ->andWhere('d.active = true')
                                ->andWhere('s.active = true');
                        },
                        'placeholder' => 'label.selectschool',
                        'attr' => [
                            'class' => 'select2-single school-select',
                        ],
                    ]);
                $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'onPreSetData']);
                $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'onPreSubmit']);
                break;
            case 3:
                $builder
                    ->add('website', TextType::class, [
                        'required' => false,
                        'label' => 'label.form.trainee.website',
                        'attr' => [
                            'placeholder' => 'label.website',
                        ],
                    ])
                    ->add('description', TextareaType::class, [
                        'required' => false,
                        'label' => 'label.form.trainee.description',
                        'attr' => [
                            'placeholder' => 'label.description',
                        ],
                    ])->add('other', TextareaType::class, [
                        'required' => false,
                        'label' => 'label.form.trainee.other',
                        'attr' => [
                            'placeholder' => 'label.other',
                        ],
                    ])->add('objectif', TextareaType::class, [
                        'required' => false,
                        'label' => 'label.form.trainee.objectif',
                        'attr' => [
                            'placeholder' => 'label.objectif',
                        ],
                    ])
                    ->add('languages', LanguageType::class, [
                        'required' => false,
                        'multiple' => true,
                        'help' => 'help.required',
                        'label' => 'Language',
                        'attr' => [
                            'class' => 'hidden',
                        ],
                        'choice_translation_locale' => $this->request->getSession()->get('_locale'),
                    ]);
                break;
            case 4:
                $builder
                    ->add('link', TextType::class, [
                        'required' => false,
                        'label' => 'label.form.trainee.video',
                        'attr' => [
                            'placeholder' => 'label.video',
                        ],
                    ])
                    ->add('documents', CollectionType::class, [
                        'entry_type' => DocumentType::class,
                        'allow_add' => true,
                        'allow_delete' => true,
                        'required' => false,
                        'label'=>false,
                        'by_reference' => false,
                        'disabled' => false,
                        'constraints' => array(
                            new Count(array('min'=>1, 'max' => 3)),
                        )
                    ])
                    ->add('avatarFile', FileType::class, [
                        'required' => false,
                        'label' => false,
                        'attr' => [
                            'class' => 'hidden images-field',
                            'accept' => 'image/jpeg,image/jpg,image/png,image/webp,image/tiff',
                        ],
                        'constraints' => [
                            new File([
                                'maxSize' => '10M'
                            ])
                        ]
                    ]);
                break;
        }
    }

    protected function addElements(FormInterface $form, $id)
    {
        // 4. Add the province element
        $form->add('degree', EntityType::class, [
            'required' => true,
            'help' => 'help.required',
            'label' => 'label.selectdegree',
            'class' => Degree::class,
            'placeholder' => 'label.selectschoolbefore',
            'validation_groups' => false,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('d')
                    ->andWhere('d.active = true')
                    ->andWhere('s.active = true')
                    ->innerJoin('d.school', 's');
            },

            'attr' => [
                'class' => 'select2-single degree-list',
            ],
        ]);
    }

    public function onPreSetData(FormEvent $event)
    {
        $form = $event->getForm();
        /**
         * @var Trainee $trainee
         */
        $trainee = $event->getData();
        if (null != $trainee->getDegree()) {
            $this->addElements($form, $trainee->getDegree()->getId() ?? -1);
        }
        $this->addElements($form, -1);
    }

    public function onPreSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
        $this->addElements($form, $data['school']);
        $data["phone"] = $this->numberUtil->parse($data['phone']['number'], $data['phone']['country']);
        $form->setData($data);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'locale' => '',
            'update' => false,
        ]);
    }
}
