<?php

namespace App\Form\Trainee;

use App\Repository\School\DegreeRepository;
use App\Repository\SettingRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class TraineeSearchType extends AbstractType
{

    private $jobs = [];
    private $translator;

    public function __construct(TranslatorInterface $translator, DegreeRepository $degreeRepository)
    {
        $this->translator = $translator;
        $array = $degreeRepository->findAllJob();
        foreach($array as $job){
            $this->jobs[$job["job"]] = $job["job"];
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('job', ChoiceType::class, [
                'label' => 'label.job',
                'required' => false,
                'multiple' => true,
                'choices' => $this->jobs,
                'attr' => [
                    'class' => 'select2-multiple-no-search',
                    'data-placeholder' => $this->translator->trans('placeholder.job')
                ],
            ])
            ->add('zone', ChoiceType::class, [
                'label' => 'label.area.geographicalarea',
                'required' => false,
                'multiple' => true,
                'choices' => [
                    'label.area.regional' => 'Regional',
                    'label.area.national' => 'National',
                    'label.area.international' => 'International',
                ],
                'attr' => [
                    'class' => 'select2-multiple-no-search',
                    'data-placeholder' => $this->translator->trans('label.area.default')
                ],
            ])
            ->add('startPeriod', TextType::class, [
                'label' => false,
            ])
            ->add('endPeriod', TextType::class, [
                'label' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'label.search',
                'attr' => [
                    'class' => 'invert-button',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}
