<?php

namespace App\Form\Trainee;

use App\Form\RegisterType;
use Craue\FormFlowBundle\Form\FormFlow;
use Symfony\Contracts\Translation\TranslatorInterface;

class TraineeFlow extends FormFlow
{
    protected $allowDynamicStepNavigation = true;
    public function __construct(TranslatorInterface $translator)
    {
        $translator->trans('label.user');
        $translator->trans('label.informations');
        $translator->trans('label.profil');
        $translator->trans('label.document');
    }
    protected function loadStepsConfig(): array
    {
        return [
            [
                'label' => 'label.user',
                'form_type' => RegisterType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ],
            [
                'label' => 'label.informations',
                'form_type' => TraineeType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ],
            [
                'label' => 'label.profil',
                'form_type' => TraineeType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                    'locale' => 'dde'
                ],
            ],
            [
                'label' => 'label.document',
                'form_type' => TraineeType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function isStepDone($stepNumber): bool
    {
        if ($this->getFormData() != null && $this->getFormData()->getId()) {
            return true;
        }

        return parent::isStepDone($stepNumber);
    }
}
