<?php

namespace App\Form\Compagny;

use App\Entity\Compagny\CompagnyInternship;
use App\Repository\SettingRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;

class CompagnyInternshipType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator, SettingRepository $settingRepository)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'label.title',
                'help' => 'help.required',
                'attr' => [
                    'placeholder' => 'label.title',
                ],
            ])
            // Plein time etc...
            ->add('workingTime', TextType::class, [
                'label' => 'label.workingtime',
                'help' => 'help.required',
                'attr' => [
                    'placeholder' => 'label.workingtime',
                ],
            ])
            // TODO UPDATE location to adress select for internship filter
            ->add('location', TextType::class, [
                'label' => 'label.location',
                'help' => 'help.required',
                'attr' => [
                    'placeholder' => 'label.location',
                ],
            ])
            ->add('vacancies', NumberType::class, [
                'label' => 'label.vacancies',
                'help' => 'help.required',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'label.form.internship.description',
                'help' => 'help.required',
                'attr' => [
                    'placeholder' => 'label.form.internship.description',
                ],
            ])
            ->add('startPeriod', TextType::class, [
                'label' => false,
            ])
            ->add('endPeriod', TextType::class, [
                'label' => false,
            ])
            ->add('searchProfil', TextareaType::class, [
                'label' => 'label.searchprofil',
                'required' => false,
                'attr' => [
                    'placeholder' => 'label.searchprofil',
                    'maxlength' => 1500
                ],
            ])
            ->add('prerequisite', TextareaType::class, [
                'label' => 'label.prerequisite',
                'required' => false,
                'attr' => [
                    'placeholder' => 'label.prerequisite',
                ],
            ])
            ->add('another', TextareaType::class, [
                'label' => 'label.form.internship.another',
                'required' => false,
                'attr' => [
                    'placeholder' => 'label.form.internship.another',
                ],
            ])
            ->add('file', FileType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'accept' => 'application/pdf',
                    'lang' => 'en',
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '40M'
                    ])
                ]
            ])
            ->add('deleteFile', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'label.active',
                'label_attr' => [
                    'class' => 'danger-text',
                ],
                'required' => false,
                'data' => true
             ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CompagnyInternship::class,
        ]);
    }
}
