<?php

namespace App\Form\Compagny;

use App\Entity\Compagny\Compagny;
use App\Form\ImageType;
use libphonenumber\PhoneNumberUtil;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class CompagnyType extends AbstractType
{
    private PhoneNumberUtil $numberUtil;
    private $translator;

    public function __construct(TranslatorInterface $translator, PhoneNumberUtil $phoneNumberUtil)
    {
        $this->translator = $translator;
        $this->numberUtil = $phoneNumberUtil;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        switch ($options['flow_step']) {
            case 2:
                $builder
                    ->add('name', TextType::class, [
                        'label' => 'label.form.company.name',
                        'help' => 'help.required',
                        'attr' => [
                            'placeholder' => 'label.name',
                        ],
                    ])
                    ->add('contactFirstname', TextType::class, [
                        'label' => 'label.contactfirstname',
                        'help' => 'help.required',
                        'attr' => [
                            'placeholder' => 'label.contactfirstname',
                        ],
                    ])
                    ->add('contactLastname', TextType::class, [
                        'label' => 'label.contactlastname',
                        'help' => 'help.required',
                        'attr' => [
                            'placeholder' => 'label.contactlastname',
                        ],
                    ])
                    ->add('contactMail', EmailType::class, [
                        'label' => 'label.contactmail',
                        'help' => 'help.required',
                        'attr' => [
                            'placeholder' => 'label.contactmail',
                        ],
                    ])
                    ->add('contactPhone', PhoneNumberType::class, array('widget' => PhoneNumberType::WIDGET_COUNTRY_CHOICE, 'country_choices' => array('AT', 'BE', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'HU', 'IE', 'IT', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE'), 'preferred_country_choices' => array('FR', 'DE', 'IT', 'LT', 'PT', 'ES')))
                    ->add('address', TextType::class, [
                        'label' => 'label.address',
                        'help' => 'help.required',
                        'attr' => [],
                    ])
                    ->add('postalcode', TextType::class, [
                        'label' => 'label.postalcode',
                        'help' => 'help.required',
                        'attr' => [],
                    ])
                    ->add('city', TextType::class, [
                        'label' => 'label.city',
                        'help' => 'help.required',
                        'attr' => [],
                    ])
                    ->add('country', TextType::class, [
                        'label' => 'label.country',
                        'help' => 'help.required',
                        'attr' => [],
                    ])
                ;
                $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'onPreSubmit']);
                break;
            case 3:
                $builder->add('website', TextType::class, [
                    'required' => false,
                    'label' => 'label.form.company.website',
                    'attr' => [],
                ])
                    ->add('description', TextareaType::class, [
                        'required' => false,
                        'label' => 'label.description',
                        'attr' => [],
                    ])
                    ->add('logoFile', FileType::class, [
                        'required' => false,
                        'label' => false,
                        'attr' => [
                            'class' => 'hidden images-field',
                            'accept' => 'image/png',
                        ],
                        'constraints' => [
                            new File([
                                'maxSize' => '10M'
                            ])
                        ]
                    ])
                    ->add('images', CollectionType::class, [
                        'required' => false,
                        'entry_type' => ImageType::class,
                        'allow_add' => true,
                        'allow_delete' => true,
                        'required' => false,
                        'label' => false,
                        'by_reference' => false,
                    ]);
                break;
        }
    }

    public function onPreSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
        $data["contactPhone"] = $this->numberUtil->parse($data['contactPhone']['number'], $data['contactPhone']['country']);
        $form->setData($data);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Compagny::class,
            'update' => false,
        ]);
    }
}
