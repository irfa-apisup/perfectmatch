<?php

namespace App\Form\Compagny;

use App\Repository\School\DegreeRepository;
use App\Repository\SettingRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InternshipSearchType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'label.name',
                ],
            ])
            ->add('startPeriod', TextType::class, [
                'label' => false,
            ])
            ->add('endPeriod', TextType::class, [
                'label' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'label.search',
                'attr' => [
                    'class' => 'invert-button',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}
