<?php

namespace App\Form\Compagny;

use App\Form\RegisterType;
use Craue\FormFlowBundle\Form\FormFlow;
use Symfony\Contracts\Translation\TranslatorInterface;

class CompagnyFlow extends FormFlow
{
    protected $allowDynamicStepNavigation = true;

    public function __construct(TranslatorInterface $translator)
    {
        $translator->trans('label.user');
        $translator->trans('label.compagny');
        $translator->trans('label.other');
    }

    protected function loadStepsConfig()
    {
        return [
            [
                'label' => 'label.user',
                'form_type' => RegisterType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ],
            [
                'label' => 'label.compagny',
                'form_type' => CompagnyType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ],
            [
                'label' => 'label.other',
                'form_type' => CompagnyType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function isStepDone($stepNumber): bool
    {
        if (null != $this->getFormData() && $this->getFormData()->getId()) {
            return true;
        }

        return parent::isStepDone($stepNumber);
    }
}
