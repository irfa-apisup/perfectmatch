<?php

namespace App\Form;

use App\Entity\Survey;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SurveyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('quality', HiddenType::class, [
                'data' => '5',
            ])
            ->add('Satisfaction', HiddenType::class, [
                'data' => '5',
            ])
            ->add('Facility', HiddenType::class, [
                'data' => '5',
            ])
            ->add('Relevance', HiddenType::class, [
                'data' => '5',
            ])
            ->add('Improvement', TextareaType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Survey::class,
        ]);
    }
}
