<?php

namespace App\Form;

use LogicException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Form\ChoiceList\Loader\IntlCallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Languages;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LanguageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choice_loader' => function (Options $options) {
                $choiceTranslationLocale = $options['choice_translation_locale'];
                $useAlpha3Codes = $options['alpha3'];
                $choiceSelfTranslation = $options['choice_self_translation'];

                return ChoiceList::loader($this, new IntlCallbackChoiceLoader(function () use ($choiceTranslationLocale, $useAlpha3Codes) {
                    $languagesList = $useAlpha3Codes ? Languages::getAlpha3Names($choiceTranslationLocale) : Languages::getNames($choiceTranslationLocale);
                    $languages = [];
                    $definedLanguages = [
                        "bg", "hr", "cs", "da", "nl", "en", "et", "fi", "fr", "de", "el", "hu", "ga", "it", "lt", "lv", "mt", "pl", "pt", "ro", "sk", "sl", "es", "sv"
                    ];
                    foreach ($languagesList as $k => $lang) {
                        if(in_array($k, $definedLanguages)){
                            $languages[$k.'a1'] = $lang.' - A1';
                            $languages[$k.'a2'] = $lang.' - A2';
                            $languages[$k.'b1'] = $lang.' - B1';
                            $languages[$k.'b2'] = $lang.' - B2';
                            $languages[$k.'c1'] = $lang.' - C1';
                            $languages[$k.'c2'] = $lang.' - C2';
                        }
                    }

                    return array_flip($languages);
                }), [$choiceTranslationLocale, $useAlpha3Codes, $choiceSelfTranslation]);
            },
            'choice_translation_domain' => false,
            'choice_translation_locale' => null,
            'alpha3' => false,
            'choice_self_translation' => false,
        ]);

        $resolver->setAllowedTypes('choice_self_translation', ['bool']);
        $resolver->setAllowedTypes('choice_translation_locale', ['null', 'string']);
        $resolver->setAllowedTypes('alpha3', 'bool');

        $resolver->setNormalizer('choice_self_translation', function (Options $options, $value) {
            if (true === $value && $options['choice_translation_locale']) {
                throw new LogicException('Cannot use the "choice_self_translation" and "choice_translation_locale" options at the same time. Remove one of them.');
            }

            return $value;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return ChoiceType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'language';
    }
}
