<?php

namespace App\Form;

use App\Entity\Compagny\Compagny;
use App\Entity\School\School;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'label.name',
                ],
            ])
            ->add('type', ChoiceType::class, [
                'label' => false,
                'choices' => [
                    'label.selectType' => '',
                    'label.school' => School::class,
                    'label.compagny' => Compagny::class,
                ],
                'required' => false,
                'attr' => [
                    'placeholder' => 'label.name',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'label.search',
                'attr' => [
                    'class' => 'invert-button',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}
