<?php

namespace App\Form;

use App\Entity\Compagny\CompagnyImage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints as Assert;

class ImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'hidden images-field',
                    'accept' => 'image/jpeg,image/jpg,image/png,image/webp,image/tiff',
                ],
                'constraints' => [
                    new Assert\Image(['mimeTypes' => ['image/jpeg','image/jpg','image/png','image/webp','image/tiff'], 'mimeTypesMessage' => 'errors.jpg']),
                    new File([
                        'maxSize' => '10M'
                    ])
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompagnyImage::class,
        ]);
    }
}
