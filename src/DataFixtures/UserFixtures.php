<?php

namespace App\DataFixtures;

use App\Constants\Permissions;
use App\Constants\Settings;
use App\Entity\Compagny\Compagny;
use App\Entity\InternshipPeriod;
use App\Entity\School\Degree;
use App\Entity\School\School;
use App\Entity\Statistics;
use App\Entity\Trainee\Trainee;
use DateInterval;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private $hash;
    private $settings;
    private PhoneNumberUtil $numberUtil;

    public function __construct(UserPasswordHasherInterface $hash, Settings $settings, PhoneNumberUtil $numberUtil)
    {
        $this->hash = $hash;
        $this->settings = $settings;
        $this->numberUtil = $numberUtil;
    }

    public function load(ObjectManager $manager): void
    {
        $this->generateTestUser($manager);
        $manager->flush();
    }

    private function generateTestUser($manager)
    {
        $phone = $this->numberUtil->parse("+33635656567", "FR");
        $manager->persist($this->settings->getAnonimisation());
        $admin = new School();
        $password = $this->hash->hashPassword($admin, 'Admin80!');
        $admin->setEmail('admin@hotmail.com');
        $admin->setPassword($password);

        $admin->setName('IRFA-APISUP');
        $admin->setContactFirstname('Renaud');
        $admin->setContactLastname('Delaplace');
        $admin->setContactMail('r.delaplace@irfa-apisup.fr');
        $admin->setContactPhone($phone);
        $admin->setAddress('55 rue sully');
        $admin->setPostalcode('80000');
        $admin->setCountry('France');
        $admin->setCity('Amiens');
        $admin->setWebsite('https://irfa-apisup.fr/');
        $admin->setDescription('Est ullamco non laboris laboris sint ullamco sint sint proident culpa reprehenderit. Culpa anim dolor officia quis. Aliquip esse dolor cupidatat incididunt et ipsum excepteur nisi minim ut consectetur. Do do adipisicing nulla aliquip nulla.');
        $admin->setUpdatedAt(new DateTime('now'));
        $admin->setCreatedAt(new DateTime('now'));
        $admin->setOrganisationtype('School');
        $admin->setRoles(Permissions::ROLE_ADMIN);
        $admin->setActive(true);

        $this->generateStatistics($admin, $manager);
        $manager->persist($admin);
        $manager->flush();

        $compagny = new Compagny();
        $password = $this->hash->hashPassword($compagny, 'Compagny80!');
        $compagny->setEmail('compagny@hotmail.com');
        $compagny->setPassword($password);

        $compagny->setName('Iteracode');
        $compagny->setContactFirstname('Benoit');
        $compagny->setContactLastname('blabla');
        $compagny->setContactMail('benoit.blabla@hotmail.com');
        $compagny->setContactPhone($phone);
        $compagny->setAddress('55 rue de sais pas');
        $compagny->setPostalcode('80000');
        $compagny->setCountry('France');
        $compagny->setCity('Amiens');
        $compagny->setWebsite('https://iteracode.fr/');
        $compagny->setDescription('Est ullamco non laboris laboris sint ullamco sint sint proident culpa reprehenderit. Culpa anim dolor officia quis. Aliquip esse dolor cupidatat incididunt et ipsum excepteur nisi minim ut consectetur. Do do adipisicing nulla aliquip nulla.');
        $compagny->setUpdatedAt(new DateTime('now'));
        $compagny->setCreatedAt(new DateTime('now'));
        $compagny->setRoles(Permissions::ROLE_COMPAGNY);
        $compagny->setActive(true);

        $this->generateStatistics($compagny, $manager);
        $manager->persist($compagny);

        $school = new School();
        $password = $this->hash->hashPassword($school, 'School80!');
        $school->setEmail('school@hotmail.com');
        $school->setPassword($password);

        $school->setName('LA MANU');
        $school->setContactFirstname('john');
        $school->setContactLastname('Doe');
        $school->setContactMail('john.doe@hotmail.om');
        $school->setContactPhone($phone);
        $school->setAddress('78 rue des jacobins');
        $school->setPostalcode('80000');
        $school->setCountry('France');
        $school->setOrganisationtype('School');
        $school->setCity('Amiens');
        $school->setWebsite('https://la-manu.fr/');
        $school->setDescription('Est ullamco non laboris laboris sint ullamco sint sint proident culpa reprehenderit. Culpa anim dolor officia quis. Aliquip esse dolor cupidatat incididunt et ipsum excepteur nisi minim ut consectetur. Do do adipisicing nulla aliquip nulla.');
        $school->setUpdatedAt(new DateTime('now'));
        $school->setCreatedAt(new DateTime('now'));
        $school->setRoles(Permissions::ROLE_SCHOOL);
        $school->setActive(true);
        $this->generateStatistics($school, $manager);

        $manager->persist($school);

        $degree = new Degree();
        $degree->setActive(true);
        $degree->setDescription('Sunt duis aliqua dolore officia. Anim tempor aliquip tempor est aliquip sint exercitation Lorem. Non officia nostrud officia ipsum consequat Lorem nisi amet. Adipisicing et dolor magna ipsum dolore dolor excepteur.');
        $degree->setName('Concepteur développeur d\'application');
        $degree->setSchool($school);
        $degree->setJob('Concepteur développeur d\'application');
        $manager->persist($degree);

        $trainee = new Trainee();
        $password = $this->hash->hashPassword($trainee, 'Trainee80!');
        $trainee->setEmail('trainee@hotmail.com');
        $trainee->setPassword($password);
        $trainee->setFirstname('Renaud');
        $trainee->setPhone($phone);
        $trainee->setLastname('Delaplace');
        $trainee->setWebsite('https://renaud.fr/');
        $trainee->setDescription('Ad incididunt reprehenderit ex cupidatat nulla sint aliqua enim. Exercitation deserunt exercitation eiusmod nulla tempor excepteur voluptate laboris ex. Reprehenderit velit dolore nisi qui tempor consectetur nisi labore. Duis culpa elit in ipsum laboris aliquip commodo ipsum elit cillum sit.');
        $trainee->setOther('Aute incididunt adipisicing veniam est exercitation fugiat qui ullamco. Tempor duis pariatur esse tempor incididunt adipisicing ea magna. Incididunt duis anim exercitation aute ullamco officia. In fugiat aute Lorem excepteur minim. Mollit do sit mollit velit mollit est nulla sunt culpa.');
        $trainee->setLink('https://moncvvideo.fr/');
        $trainee->setObjectif('Laborum est ut elit reprehenderit. Voluptate deserunt labore ex dolor quis consequat consequat occaecat culpa. Consectetur voluptate non incididunt do reprehenderit velit sit nisi qui consectetur eu elit quis. Adipisicing ea nostrud non quis commodo nostrud. Ea enim minim do ex exercitation est sint irure cupidatat qui. Ad do mollit irure irure incididunt.');
        $trainee->setZone(['International']);
        $trainee->setDegree($degree);
        $trainee->setUpdatedAt(new DateTime('now'));
        $period = new InternshipPeriod();
        $date = new DateTime('now');
        $period->setStart($date);
        $period->setEnd($date->add(DateInterval::createFromDateString('7 day')));
        $trainee->setSchool($school);
        $trainee->setActive(true);
        $trainee->setInitialized(true);
        $trainee->setRoles(Permissions::ROLE_TRAINEE);

        $this->generateStatistics($trainee, $manager);
        $manager->persist($trainee);
    }

    private function generateStatistics($user, $manager)
    {
        $statistics = new Statistics();
        $statistics->setOwner($user);
        $manager->persist($statistics);
    }
}
