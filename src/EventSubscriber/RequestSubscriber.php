<?php

namespace App\EventSubscriber;

use App\Entity\Trainee\Trainee;
use App\Repository\UserRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class RequestSubscriber implements EventSubscriberInterface
{
    private TokenStorageInterface $tokenStorage;
    private RouterInterface $router;
    private UserRepository $userRepository;

    public function __construct(TokenStorageInterface $tokenStorage, RouterInterface $router, UserRepository $userRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
        $this->userRepository =  $userRepository;
    }

    public function onKernelRequest(RequestEvent $event)
    {

        $routeName = $event->getRequest()->get('_route');
        if ($event->isMainRequest() && 'account_settings' == $routeName) {
            return;
        }

        if ($event->isMainRequest() && 'app_referer' != $routeName) {
            $users = $this->userRepository->userExist();
            if ($users == 0) {
                $response = new RedirectResponse($this->router->generate('app_referer'));
                $event->setResponse($response);
            }
        }else{
            return;
        }

        if (!$token = $this->tokenStorage->getToken()) {
            return;
        }

        $sessionUser = $token->getUser();

        if ($sessionUser instanceof Trainee) {
            /**
             * @var Trainee $trainee
             */
            $trainee = $sessionUser;
            if (!$trainee->getInitialized()) {
                $response = new RedirectResponse($this->router->generate('account_settings'));
                $event->setResponse($response);
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.request' => 'onKernelRequest',
        ];
    }
}
