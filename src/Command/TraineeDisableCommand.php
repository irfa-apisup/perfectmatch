<?php

namespace App\Command;

use App\Repository\Trainee\TraineeRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class TraineeDisableCommand extends Command
{
    private $traineeRepository;

    protected static $defaultName = 'app:trainee:disable';

    public function __construct(TraineeRepository $traineeRepository)
    {
        $this->traineeRepository = $traineeRepository;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Disable inactive account')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Dry run');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if ($input->getOption('dry-run')) {
            $io->note('Dry mode enabled');
            $count = $this->traineeRepository->countInactive();
        } else {
            $count = $this->traineeRepository->disableInactive();
        }

        $io->success(sprintf('Disable "%d" inactive account.', $count));

        return 0;
    }
}
