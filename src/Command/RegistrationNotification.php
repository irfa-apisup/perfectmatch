<?php

namespace App\Command;

use App\Common\MailerHelper;
use App\Repository\Compagny\CompagnyRepository;
use App\Repository\School\SchoolRepository;
use App\Repository\Trainee\TraineeRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RegistrationNotification extends Command
{
    private SchoolRepository $schoolRepository;
    private CompagnyRepository $companyRepository;
    private MailerHelper $mailerHelper;

    protected static $defaultName = 'app:registration:notify';

    public function __construct(SchoolRepository $schoolRepository, CompagnyRepository $compagnyRepository, MailerHelper $mailerHelper)
    {
        $this->schoolRepository = $schoolRepository;
        $this->companyRepository = $compagnyRepository;
        $this->mailerHelper = $mailerHelper;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Notify referent school for new registration')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Dry run');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $count = 0;
        if ($input->getOption('dry-run')) {
            $io->note('Dry mode enabled');
            $count = $this->companyRepository->countNewDailyRegistration();
            $count += $this->schoolRepository->countNewDailyRegistration();
        } else {
            $company = $this->companyRepository->countNewDailyRegistration();
            $school = $this->schoolRepository->countNewDailyRegistration();
            $count = $company + $school;

            $this->mailerHelper->sendRegistrationNotification($school, $company);
        }

        $io->success(sprintf('Today "%d" entries have been made.', $count));

        return 0;
    }
}
