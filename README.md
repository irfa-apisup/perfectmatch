
PerfectMatch
==========

A small documentation for [PerfectMatch][1] Application installation.

[![Open Source? Yes!](https://badgen.net/badge/Open%20Source%20%3F/Yes%21/blue?icon=github)](#)
[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC_BY--NC--SA_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

Table of Contents
-----------------

* [Requirements](#requirements)
* [Installation](#installation)
* [License](#license)

Requirements
------------

PerfectMatch requires the following to run:

* [Symfony][symfony] 5.4
* [PhP][php] 8.0+
* [Npm][npm] 
* [Composer][composer]  2+
* [MariaDB][mariadb]  10.6.5+

Installation
-----

1. Download application package or clone the repository

```sh
git clone https://gitlab.com/irfa-apisup/perfectmatch.git
```

2. [Configuring][web-server] a Web Server


3. Make sure web server can write to app


5. Configure Mariadb connection, by default it will connect with localhost, you can change it by editing app/.env

```sh
DATABASE_URL="mysql://<user>:<password>@127.0.0.1:3306/perfectmatch?serverVersion=mariadb-10.6.5"
```

5. Configure SMTP connection, you can change it by editing app/.env

```sh
MAILER_DSN=smtp://localhost
FROM_RECIPIENT=from@perfectmatch.fr
EMITTER=noreply@perfectmatch.fr
```
- And after create the database:
```sh
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

6. (Optional) To use [Loco][loco] ([API-KEY][help-loco]) and set up translations from a web interface, you can change it by editing app/.env

```sh
LOCO_PROJECT_API_KEY=<API-KEY>
```

7. Initialize project dependencies

```sh
npm install or yarn install
composer require
```

8. Setup cronjob for disable inactive user
```sh
00 00 * * * php app/bin/console app:trainee:disable  
```

9. Setup cronjob for send registration summary
```sh
00 00 * * * php app/bin/console app:registration:notify  
```

10. Now go on the application address and create the administrator

License
-------

PerfectMatch is licensed under the [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.  

[1]: https://www.perfectmatch-erasmus.com/
[php]: https://www.php.net
[npm]: https://www.npmjs.com/
[symfony]: https://symfony.com
[composer]: https://getcomposer.org
[mariadb]: https://mariadb.com/fr/
[web-server]: https://symfony.com/doc/5.4/setup/web_server_configuration.html
[loco]: https://localise.biz
[help-loco]: https://localise.biz/help/developers/api-keys
