#!/bin/bash

# Fonction pour tester la connexion au serveur MariaDB
function testMariaDBConnection() {
    mysql -u root -e "SELECT 1;" > /dev/null 
    return $?
}

# Chemin vers le fichier .env
env_file="/var/www/html/.env"
db_path="mysql://$MYSQL_USERNAME:$MYSQL_PASSWORD@localhost:$MYSQL_PORT/$MYSQL_DATABASE?serverVersion=mariadb-10.6.5"

echo "Update environment variables."
# Parcourir toutes les variables d'environnement du conteneur
while IFS='=' read -r key value; do
  # Vérifier si la variable d'environnement commence par "PM_"
  if [[ $key == PM_* ]]; then
    # Extraire le nom de la variable sans le préfixe "PM_"
    var_name="${key#PM_}"
    
    # Mettre à jour la valeur correspondante dans le fichier .env
    sed -i "s|^$var_name=.*$|$var_name=$value|g" "$env_file"
  fi
done < <(env)

sed -i "s|^DATABASE_URL=.*$|DATABASE_URL=$db_path|g" "$env_file"

echo -e "[mysqld]\nport = $MYSQL_PORT\nbind-address = localhost" > /etc/my.cnf.d/my.cnf

mysqld_safe --defaults-file=/etc/my.cnf.d/my.cnf > /dev/null  &

# Boucle de test
MAX_RETRIES=5
RETRY_DELAY=5

for ((i=1; i<=$MAX_RETRIES; i++)); do
    echo "Connecting to MariaDB..."
    sleep $RETRY_DELAY
    if testMariaDBConnection; then
        echo "Successful connection to MariaDB."
        break
    fi

    if [ $i -eq $MAX_RETRIES ]; then
        echo "Unable to connect to MariaDB after $MAX_RETRIES attempts. Script stopped."
        #exit 1
    fi
    echo "Connection to MariaDB failed. Retry in $RETRY_DELAY seconds..."
done

#Configuration de MariaDB
echo "CREATE DATABASE IF NOT EXISTS \`$MYSQL_DATABASE\` CHARACTER SET utf8 COLLATE utf8_general_ci;" > /docker-entrypoint-initdb.d/init.sql \
&& echo "CREATE USER '$MYSQL_USERNAME'@'localhost' IDENTIFIED BY '$MYSQL_PASSWORD';" >> /docker-entrypoint-initdb.d/init.sql \
&& echo "GRANT ALL PRIVILEGES ON \`$MYSQL_DATABASE\`.* TO '$MYSQL_USERName'@'localhost';" >> /docker-entrypoint-initdb.d/init.sql \
&& echo "FLUSH PRIVILEGES;" >> /docker-entrypoint-initdb.d/init.sql

mysql -u root < /docker-entrypoint-initdb.d/init.sql 

echo 'MariaDB configuration.'
# Exécution des commandes de sécurisation
mysql_upgrade -u root --password=rootpassword > /dev/null  \
    && mysql -u root --password=rootpassword -e "DELETE FROM mysql.user WHERE User='';" \
    && mysql -u root --password=rootpassword -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%';" \
    && mysql -u root --password=rootpassword -e "FLUSH PRIVILEGES;"

# Installation des dépendances
echo 'Installation of dependencies...'
composer install --prefer-dist --no-dev --optimize-autoloader > /dev/null 2>&1

#Mise à jour des permissions
echo 'Update permissions'
chown -R www-data:www-data /var/www/html > /dev/null \
&& chmod -R 575 /var/www/html/public/images > /dev/null 
# Réglage des permissions sur les fichiers d'application
echo 'Database provisioning'
php bin/console d:m:m > /dev/null 2>&1 

#Génération du certificat ssl
echo 'Generating the ssl certificate'
certbot certonly -m $SSL_EMAIL -d $DOMAIN --agree-tos --no-eff-email --nginx

#Création des cron
echo 'Creating cronjobs'
echo "00 00 1 * * /usr/bin/certbot renew --quiet" >> /var/spool/cron/crontabs/root \
&& echo "00 00 * * * php app/bin/console app:trainee:disable  " >> /var/spool/cron/crontabs/root \
&& echo "00 00 * * * php app/bin/console app:registration:notify " >> /var/spool/cron/crontabs/root

# Lance les processus
echo 'Server and reverse proxy startup'
nginx -g 'daemon off;' &
php-fpm